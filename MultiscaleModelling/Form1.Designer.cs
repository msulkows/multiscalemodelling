﻿namespace MultiscaleModelling
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numberOfGrainsTextBox = new System.Windows.Forms.TextBox();
            this.VonNeumannCheckbox = new System.Windows.Forms.CheckBox();
            this.MooreCheckbox = new System.Windows.Forms.CheckBox();
            this.AbsorbingCheckbox = new System.Windows.Forms.CheckBox();
            this.PeriodicCheckbox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.NucleatingButton = new System.Windows.Forms.Button();
            this.GrowthButton = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.microstructureMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.importMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.bitmapImportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.textImportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.exportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.bitmapExportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.textExportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.amountOfInclusionsTextBox = new System.Windows.Forms.TextBox();
            this.sizeOfInclusionsTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SquareCheckBox = new System.Windows.Forms.CheckBox();
            this.CircularCheckBox = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.BeforeCheckBox = new System.Windows.Forms.CheckBox();
            this.AfterCheckBox = new System.Windows.Forms.CheckBox();
            this.Moore2Checkbox = new System.Windows.Forms.CheckBox();
            this.percentageTextBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SubstructureCheckBox = new System.Windows.Forms.CheckBox();
            this.DualPhaseCheckBox = new System.Windows.Forms.CheckBox();
            this.label10 = new System.Windows.Forms.Label();
            this.BoundariesCheckBox = new System.Windows.Forms.CheckBox();
            this.GBSizeTextBox = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.SelectedBoundariesCheckBox = new System.Windows.Forms.CheckBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ClearAllButton = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.GBLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(24, 46);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 500);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(595, 104);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of grains";
            // 
            // numberOfGrainsTextBox
            // 
            this.numberOfGrainsTextBox.Location = new System.Drawing.Point(700, 101);
            this.numberOfGrainsTextBox.Name = "numberOfGrainsTextBox";
            this.numberOfGrainsTextBox.Size = new System.Drawing.Size(56, 20);
            this.numberOfGrainsTextBox.TabIndex = 2;
            // 
            // VonNeumannCheckbox
            // 
            this.VonNeumannCheckbox.AutoSize = true;
            this.VonNeumannCheckbox.Location = new System.Drawing.Point(598, 158);
            this.VonNeumannCheckbox.Name = "VonNeumannCheckbox";
            this.VonNeumannCheckbox.Size = new System.Drawing.Size(94, 17);
            this.VonNeumannCheckbox.TabIndex = 3;
            this.VonNeumannCheckbox.Text = "Von Neumann";
            this.VonNeumannCheckbox.UseVisualStyleBackColor = true;
            // 
            // MooreCheckbox
            // 
            this.MooreCheckbox.AutoSize = true;
            this.MooreCheckbox.Location = new System.Drawing.Point(700, 158);
            this.MooreCheckbox.Name = "MooreCheckbox";
            this.MooreCheckbox.Size = new System.Drawing.Size(56, 17);
            this.MooreCheckbox.TabIndex = 4;
            this.MooreCheckbox.Text = "Moore";
            this.MooreCheckbox.UseVisualStyleBackColor = true;
            // 
            // AbsorbingCheckbox
            // 
            this.AbsorbingCheckbox.AutoSize = true;
            this.AbsorbingCheckbox.Location = new System.Drawing.Point(598, 270);
            this.AbsorbingCheckbox.Name = "AbsorbingCheckbox";
            this.AbsorbingCheckbox.Size = new System.Drawing.Size(73, 17);
            this.AbsorbingCheckbox.TabIndex = 5;
            this.AbsorbingCheckbox.Text = "Absorbing";
            this.AbsorbingCheckbox.UseVisualStyleBackColor = true;
            // 
            // PeriodicCheckbox
            // 
            this.PeriodicCheckbox.AutoSize = true;
            this.PeriodicCheckbox.Location = new System.Drawing.Point(700, 270);
            this.PeriodicCheckbox.Name = "PeriodicCheckbox";
            this.PeriodicCheckbox.Size = new System.Drawing.Size(64, 17);
            this.PeriodicCheckbox.TabIndex = 6;
            this.PeriodicCheckbox.Text = "Periodic";
            this.PeriodicCheckbox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(595, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Neighbourhoods types";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(595, 245);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Boundary conditions";
            // 
            // NucleatingButton
            // 
            this.NucleatingButton.Location = new System.Drawing.Point(830, 395);
            this.NucleatingButton.Name = "NucleatingButton";
            this.NucleatingButton.Size = new System.Drawing.Size(75, 23);
            this.NucleatingButton.TabIndex = 9;
            this.NucleatingButton.Text = "Nucleating";
            this.NucleatingButton.UseVisualStyleBackColor = true;
            this.NucleatingButton.Click += new System.EventHandler(this.NucleatingButton_Click);
            // 
            // GrowthButton
            // 
            this.GrowthButton.Location = new System.Drawing.Point(830, 423);
            this.GrowthButton.Name = "GrowthButton";
            this.GrowthButton.Size = new System.Drawing.Size(75, 23);
            this.GrowthButton.TabIndex = 10;
            this.GrowthButton.Text = "Growth";
            this.GrowthButton.UseVisualStyleBackColor = true;
            this.GrowthButton.Click += new System.EventHandler(this.GrowthButton_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(830, 452);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(75, 23);
            this.ClearButton.TabIndex = 11;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1020, 24);
            this.menuStrip1.TabIndex = 12;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.microstructureMenu});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "File";
            // 
            // microstructureMenu
            // 
            this.microstructureMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importMenu,
            this.exportMenu});
            this.microstructureMenu.Name = "microstructureMenu";
            this.microstructureMenu.Size = new System.Drawing.Size(152, 22);
            this.microstructureMenu.Text = "Microstructure";
            // 
            // importMenu
            // 
            this.importMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bitmapImportMenu,
            this.textImportMenu});
            this.importMenu.Name = "importMenu";
            this.importMenu.Size = new System.Drawing.Size(110, 22);
            this.importMenu.Text = "Import";
            // 
            // bitmapImportMenu
            // 
            this.bitmapImportMenu.Name = "bitmapImportMenu";
            this.bitmapImportMenu.Size = new System.Drawing.Size(112, 22);
            this.bitmapImportMenu.Text = "Bitmap";
            this.bitmapImportMenu.Click += new System.EventHandler(this.bitmapImportMenu_Click);
            // 
            // textImportMenu
            // 
            this.textImportMenu.Name = "textImportMenu";
            this.textImportMenu.Size = new System.Drawing.Size(112, 22);
            this.textImportMenu.Text = "Text";
            this.textImportMenu.Click += new System.EventHandler(this.textImportMenu_Click);
            // 
            // exportMenu
            // 
            this.exportMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bitmapExportMenu,
            this.textExportMenu});
            this.exportMenu.Name = "exportMenu";
            this.exportMenu.Size = new System.Drawing.Size(110, 22);
            this.exportMenu.Text = "Export";
            // 
            // bitmapExportMenu
            // 
            this.bitmapExportMenu.Name = "bitmapExportMenu";
            this.bitmapExportMenu.Size = new System.Drawing.Size(112, 22);
            this.bitmapExportMenu.Text = "Bitmap";
            this.bitmapExportMenu.Click += new System.EventHandler(this.bitmapExportMenu_Click);
            // 
            // textExportMenu
            // 
            this.textExportMenu.Name = "textExportMenu";
            this.textExportMenu.Size = new System.Drawing.Size(112, 22);
            this.textExportMenu.Text = "Text";
            this.textExportMenu.Click += new System.EventHandler(this.textExportMenu_Click);
            // 
            // amountOfInclusionsTextBox
            // 
            this.amountOfInclusionsTextBox.Location = new System.Drawing.Point(700, 324);
            this.amountOfInclusionsTextBox.Name = "amountOfInclusionsTextBox";
            this.amountOfInclusionsTextBox.Size = new System.Drawing.Size(56, 20);
            this.amountOfInclusionsTextBox.TabIndex = 13;
            // 
            // sizeOfInclusionsTextBox
            // 
            this.sizeOfInclusionsTextBox.Location = new System.Drawing.Point(700, 350);
            this.sizeOfInclusionsTextBox.Name = "sizeOfInclusionsTextBox";
            this.sizeOfInclusionsTextBox.Size = new System.Drawing.Size(56, 20);
            this.sizeOfInclusionsTextBox.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(595, 300);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Inclusions";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(595, 327);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Amount of inclusions";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(595, 353);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Size of inclusions";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(598, 383);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Type of inclusions";
            // 
            // SquareCheckBox
            // 
            this.SquareCheckBox.AutoSize = true;
            this.SquareCheckBox.Location = new System.Drawing.Point(598, 412);
            this.SquareCheckBox.Name = "SquareCheckBox";
            this.SquareCheckBox.Size = new System.Drawing.Size(60, 17);
            this.SquareCheckBox.TabIndex = 19;
            this.SquareCheckBox.Text = "Square";
            this.SquareCheckBox.UseVisualStyleBackColor = true;
            // 
            // CircularCheckBox
            // 
            this.CircularCheckBox.AutoSize = true;
            this.CircularCheckBox.Location = new System.Drawing.Point(700, 412);
            this.CircularCheckBox.Name = "CircularCheckBox";
            this.CircularCheckBox.Size = new System.Drawing.Size(61, 17);
            this.CircularCheckBox.TabIndex = 20;
            this.CircularCheckBox.Text = "Circular";
            this.CircularCheckBox.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(598, 442);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(137, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Grain growth and inclusions";
            // 
            // BeforeCheckBox
            // 
            this.BeforeCheckBox.AutoSize = true;
            this.BeforeCheckBox.Location = new System.Drawing.Point(598, 471);
            this.BeforeCheckBox.Name = "BeforeCheckBox";
            this.BeforeCheckBox.Size = new System.Drawing.Size(57, 17);
            this.BeforeCheckBox.TabIndex = 22;
            this.BeforeCheckBox.Text = "Before";
            this.BeforeCheckBox.UseVisualStyleBackColor = true;
            // 
            // AfterCheckBox
            // 
            this.AfterCheckBox.AutoSize = true;
            this.AfterCheckBox.Location = new System.Drawing.Point(700, 471);
            this.AfterCheckBox.Name = "AfterCheckBox";
            this.AfterCheckBox.Size = new System.Drawing.Size(48, 17);
            this.AfterCheckBox.TabIndex = 23;
            this.AfterCheckBox.Text = "After";
            this.AfterCheckBox.UseVisualStyleBackColor = true;
            // 
            // Moore2Checkbox
            // 
            this.Moore2Checkbox.AutoSize = true;
            this.Moore2Checkbox.Location = new System.Drawing.Point(700, 181);
            this.Moore2Checkbox.Name = "Moore2Checkbox";
            this.Moore2Checkbox.Size = new System.Drawing.Size(65, 17);
            this.Moore2Checkbox.TabIndex = 24;
            this.Moore2Checkbox.Text = "Moore 2";
            this.Moore2Checkbox.UseVisualStyleBackColor = true;
            // 
            // percentageTextBox
            // 
            this.percentageTextBox.Location = new System.Drawing.Point(700, 213);
            this.percentageTextBox.Name = "percentageTextBox";
            this.percentageTextBox.Size = new System.Drawing.Size(56, 20);
            this.percentageTextBox.TabIndex = 25;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(598, 213);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "percentage";
            // 
            // SubstructureCheckBox
            // 
            this.SubstructureCheckBox.AutoSize = true;
            this.SubstructureCheckBox.Location = new System.Drawing.Point(819, 158);
            this.SubstructureCheckBox.Name = "SubstructureCheckBox";
            this.SubstructureCheckBox.Size = new System.Drawing.Size(86, 17);
            this.SubstructureCheckBox.TabIndex = 27;
            this.SubstructureCheckBox.Text = "Substructure";
            this.SubstructureCheckBox.UseVisualStyleBackColor = true;
            // 
            // DualPhaseCheckBox
            // 
            this.DualPhaseCheckBox.AutoSize = true;
            this.DualPhaseCheckBox.Location = new System.Drawing.Point(819, 181);
            this.DualPhaseCheckBox.Name = "DualPhaseCheckBox";
            this.DualPhaseCheckBox.Size = new System.Drawing.Size(81, 17);
            this.DualPhaseCheckBox.TabIndex = 28;
            this.DualPhaseCheckBox.Text = "Dual Phase";
            this.DualPhaseCheckBox.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(816, 133);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(50, 13);
            this.label10.TabIndex = 29;
            this.label10.Text = "Structure";
            // 
            // BoundariesCheckBox
            // 
            this.BoundariesCheckBox.AutoSize = true;
            this.BoundariesCheckBox.Location = new System.Drawing.Point(822, 261);
            this.BoundariesCheckBox.Name = "BoundariesCheckBox";
            this.BoundariesCheckBox.Size = new System.Drawing.Size(79, 17);
            this.BoundariesCheckBox.TabIndex = 30;
            this.BoundariesCheckBox.Text = "Boundaries";
            this.BoundariesCheckBox.UseVisualStyleBackColor = true;
            // 
            // GBSizeTextBox
            // 
            this.GBSizeTextBox.Location = new System.Drawing.Point(879, 310);
            this.GBSizeTextBox.Name = "GBSizeTextBox";
            this.GBSizeTextBox.Size = new System.Drawing.Size(56, 20);
            this.GBSizeTextBox.TabIndex = 31;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(823, 310);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 32;
            this.label11.Text = "GB size";
            // 
            // SelectedBoundariesCheckBox
            // 
            this.SelectedBoundariesCheckBox.AutoSize = true;
            this.SelectedBoundariesCheckBox.Location = new System.Drawing.Point(822, 284);
            this.SelectedBoundariesCheckBox.Name = "SelectedBoundariesCheckBox";
            this.SelectedBoundariesCheckBox.Size = new System.Drawing.Size(123, 17);
            this.SelectedBoundariesCheckBox.TabIndex = 33;
            this.SelectedBoundariesCheckBox.Text = "Selected boundaries";
            this.SelectedBoundariesCheckBox.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(819, 235);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 34;
            this.label12.Text = "Boundaries";
            // 
            // ClearAllButton
            // 
            this.ClearAllButton.Location = new System.Drawing.Point(830, 482);
            this.ClearAllButton.Name = "ClearAllButton";
            this.ClearAllButton.Size = new System.Drawing.Size(75, 23);
            this.ClearAllButton.TabIndex = 35;
            this.ClearAllButton.Text = "Clear all";
            this.ClearAllButton.UseVisualStyleBackColor = true;
            this.ClearAllButton.Click += new System.EventHandler(this.ClearAllButton_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(822, 352);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(33, 13);
            this.label13.TabIndex = 36;
            this.label13.Text = "% GB";
            // 
            // GBLabel
            // 
            this.GBLabel.AutoSize = true;
            this.GBLabel.Location = new System.Drawing.Point(879, 352);
            this.GBLabel.Name = "GBLabel";
            this.GBLabel.Size = new System.Drawing.Size(0, 13);
            this.GBLabel.TabIndex = 37;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1020, 558);
            this.Controls.Add(this.GBLabel);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.ClearAllButton);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.SelectedBoundariesCheckBox);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.GBSizeTextBox);
            this.Controls.Add(this.BoundariesCheckBox);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.DualPhaseCheckBox);
            this.Controls.Add(this.SubstructureCheckBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.percentageTextBox);
            this.Controls.Add(this.Moore2Checkbox);
            this.Controls.Add(this.AfterCheckBox);
            this.Controls.Add(this.BeforeCheckBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.CircularCheckBox);
            this.Controls.Add(this.SquareCheckBox);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.sizeOfInclusionsTextBox);
            this.Controls.Add(this.amountOfInclusionsTextBox);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.GrowthButton);
            this.Controls.Add(this.NucleatingButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PeriodicCheckbox);
            this.Controls.Add(this.AbsorbingCheckbox);
            this.Controls.Add(this.MooreCheckbox);
            this.Controls.Add(this.VonNeumannCheckbox);
            this.Controls.Add(this.numberOfGrainsTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "CA Grain Growth Algorithm";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox numberOfGrainsTextBox;
        private System.Windows.Forms.CheckBox VonNeumannCheckbox;
        private System.Windows.Forms.CheckBox MooreCheckbox;
        private System.Windows.Forms.CheckBox AbsorbingCheckbox;
        private System.Windows.Forms.CheckBox PeriodicCheckbox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button NucleatingButton;
        private System.Windows.Forms.Button GrowthButton;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem microstructureMenu;
        private System.Windows.Forms.ToolStripMenuItem importMenu;
        private System.Windows.Forms.ToolStripMenuItem exportMenu;
        private System.Windows.Forms.ToolStripMenuItem bitmapImportMenu;
        private System.Windows.Forms.ToolStripMenuItem textImportMenu;
        private System.Windows.Forms.ToolStripMenuItem bitmapExportMenu;
        private System.Windows.Forms.ToolStripMenuItem textExportMenu;
        private System.Windows.Forms.TextBox amountOfInclusionsTextBox;
        private System.Windows.Forms.TextBox sizeOfInclusionsTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox SquareCheckBox;
        private System.Windows.Forms.CheckBox CircularCheckBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox BeforeCheckBox;
        private System.Windows.Forms.CheckBox AfterCheckBox;
        private System.Windows.Forms.CheckBox Moore2Checkbox;
        private System.Windows.Forms.TextBox percentageTextBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox SubstructureCheckBox;
        private System.Windows.Forms.CheckBox DualPhaseCheckBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox BoundariesCheckBox;
        private System.Windows.Forms.TextBox GBSizeTextBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox SelectedBoundariesCheckBox;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button ClearAllButton;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label GBLabel;
    }
}

