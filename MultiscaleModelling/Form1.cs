﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MultiscaleModelling
{  
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        //tables of grains and bitmap
        Grain[,] matrix = new Grain[500, 500];
        Grain[,] matrixI = new Grain[500, 500];
        Bitmap bitmap = new Bitmap(500, 500);
        bool full = false;                           // full = no white color
        bool ready = false;                          // ready = ready to export
        bool first = true;                           // first = matrix copy to matrixI on first go 
        bool firstSaved = true;                      // firstSaved = create instances savedGrains
        static Random rnd = new Random();
        Grain[,] savedGrains = new Grain[500, 500];
        int numberSaved=0;
        Color[] savedColors = new Color[500];
        bool saved = false;                         // saved = color is saved by user
        Color color;
        int boundariesSize;
        int blackBoundaries = 0;
        double boundariesPercentage = 0;


        private void NucleatingButton_Click(object sender, EventArgs e)
        {
            //initialization of tables
            if (first)
            {
                int id = 0;
                for(int i=0; i < 500; i++)
                {
                   for(int j=0; j<500; j++)
                    {
                    
                        matrix[i, j] = new Grain();
                        matrix[i, j].color = Color.White;
                        matrixI[i, j] = new Grain();
                        matrixI[i, j].color = Color.White;
                        id++;
                    }
                }
            }

            if (firstSaved)
            {
                for (int i = 0; i < 500; i++)
                {
                    for (int j = 0; j < 500; j++)
                    {
                        savedGrains[i,j] = new Grain();
                        savedGrains[i,j].color = Color.White;
                    }
                }
                firstSaved = false;
            }

            //checking if number of grains is ok
            if (!Int32.TryParse(numberOfGrainsTextBox.Text, out int numberOfGains)){
                MessageBox.Show("Bad value. Please fix it.", "Number of grains", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //randomly choosing grains and colors
            
            for (int k=0; k<numberOfGains; k++)
            {
                    int i = rnd.Next(0, 500);
                    int j = rnd.Next(0, 500);
                    color = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                    if (matrix[i, j].color == Color.White)
                    {
                        matrix[i, j].color = color;
                    }
                
            }

            // setting bitmap
            for(int i=0; i<500; i++)
            {
                for (int j=0; j < 500; j++)
                {
                        bitmap.SetPixel(i, j, matrix[i, j].color);
                    
                }
            }

            pictureBox1.Image = bitmap;
            pictureBox1.Refresh();

            first = true;
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            if(BoundariesCheckBox.Checked||SelectedBoundariesCheckBox.Checked){ 
            //checking if size of boundaries is ok
            if (!Int32.TryParse(GBSizeTextBox.Text, out int boundariesSizeFromText))
            {
                MessageBox.Show("Bad value. Please fix it.", "Size of boundaries", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
                boundariesSize = boundariesSizeFromText;
        }
            //set matrix and bitmap to white
            saved = false;
            for (int i = 0; i < 500; i++)
            {
                for (int j = 0; j < 500; j++)
                {
                    for(int k = 0; k < numberSaved; k++)
                    {
                            if (matrix[i, j].color == savedColors[k]) saved = true; 
                    }

                    if (saved)
                    {
                        if (DualPhaseCheckBox.Checked)
                        {
                            matrix[i, j].color = savedColors[0];
                            bitmap.SetPixel(i, j, savedColors[0]);
                        }
                    }

                    if (BoundariesCheckBox.Checked)
                    {

                        if (matrix[i, j].color != Color.Black)
                        {
                            if ((i - 1 >= 0) && (i + 1 < 500) && (j - 1 >= 0) && (j + 1 < 500))
                            {
                                if (
                                       ((matrix[i, j].color != matrix[i + 1, j].color) && (matrix[i + 1, j].color != Color.Black) && (matrix[i + 1, j].color != Color.White))
                                    || ((matrix[i, j].color != matrix[i - 1, j].color) && (matrix[i - 1, j].color != Color.Black) && (matrix[i - 1, j].color != Color.White))
                                    || ((matrix[i, j].color != matrix[i, j + 1].color) && (matrix[i, j + 1].color != Color.Black) && (matrix[i, j + 1].color != Color.White))
                                    || ((matrix[i, j].color != matrix[i, j - 1].color) && (matrix[i, j - 1].color != Color.Black) && (matrix[i, j - 1].color != Color.White))
                                    || ((matrix[i, j].color != matrix[i + 1, j + 1].color) && (matrix[i + 1, j + 1].color != Color.Black) && (matrix[i + 1, j + 1].color != Color.White))
                                    || ((matrix[i, j].color != matrix[i + 1, j - 1].color) && (matrix[i + 1, j - 1].color != Color.Black) && (matrix[i + 1, j - 1].color != Color.White))
                                    || ((matrix[i, j].color != matrix[i - 1, j + 1].color) && (matrix[i - 1, j + 1].color != Color.Black) && (matrix[i - 1, j + 1].color != Color.White))
                                    || ((matrix[i, j].color != matrix[i - 1, j - 1].color) && (matrix[i - 1, j - 1].color != Color.Black) && (matrix[i - 1, j - 1].color != Color.White))
                                    )
                                {
                                    matrix[i, j].color = Color.Black;
                                    bitmap.SetPixel(i, j, Color.Black);

                                    for (int k = 1; k < boundariesSize; k++)
                                    {
                                        if (i + k < 500 && j + k < 500)
                                        {
                                            matrix[i + k, j + k].color = Color.Black;
                                            bitmap.SetPixel(i + k, j + k, Color.Black);
                                        }
                                    }
  
                                }
                            }
                        }
                    }

                    if (SelectedBoundariesCheckBox.Checked)
                    {
                        for (int k = 0; k < numberSaved; k++)
                        {
                            if (matrix[i, j].color == savedColors[k])
                            {
                                if ((i - 1 >= 0) && (i + 1 < 500) && (j - 1 >= 0) && (j + 1 < 500))
                                {
                                    if (
                                        ((matrix[i, j].color != matrix[i + 1, j].color) && (matrix[i + 1, j].color != Color.Black) )
                                        || ((matrix[i, j].color != matrix[i - 1, j].color) && (matrix[i - 1, j].color != Color.Black))
                                        || ((matrix[i, j].color != matrix[i, j + 1].color) && (matrix[i, j + 1].color != Color.Black) )
                                        || ((matrix[i, j].color != matrix[i, j - 1].color) && (matrix[i, j - 1].color != Color.Black) )
                                        || ((matrix[i, j].color != matrix[i + 1, j + 1].color) && (matrix[i + 1, j + 1].color != Color.Black))
                                        || ((matrix[i, j].color != matrix[i + 1, j - 1].color) && (matrix[i + 1, j - 1].color != Color.Black) )
                                        || ((matrix[i, j].color != matrix[i - 1, j + 1].color) && (matrix[i - 1, j + 1].color != Color.Black))
                                        || ((matrix[i, j].color != matrix[i - 1, j - 1].color) && (matrix[i - 1, j - 1].color != Color.Black) )
                                        )
                                    {
                                        matrix[i, j].color = Color.Black;
                                        bitmap.SetPixel(i, j, Color.Black);

                                        for (int l = 1; l < boundariesSize; l++)
                                        {
                                            if (i + l < 500 && j + l < 500)
                                            {
                                                matrix[i + l, j + l].color = Color.Black;
                                                bitmap.SetPixel(i + l, j + l, Color.Black);
                                            }
                                        }
                                    }
                                    
                                }
                                
                            }
                        }

                    }

                    if (!saved)
                    {
                        if (matrix[i, j].color != Color.Black)
                        {
                            matrix[i, j].color = Color.White;
                            bitmap.SetPixel(i, j, Color.White);
                        }
                    }
                    saved = false;
                    
                    
                }
            }
            if (SelectedBoundariesCheckBox.Checked)
            {
                for (int i = 0; i < 500; i++)
                {
                    for (int j = 0; j < 500; j++)
                    {
                        if (matrix[i, j].color != Color.Black && matrix[i, j].color != Color.White)
                        {
                            matrix[i, j].color = Color.White;
                            bitmap.SetPixel(i, j, Color.White);
                        }
                    }
                }
            }

            //get % of boundaries
            if(BoundariesCheckBox.Checked || SelectedBoundariesCheckBox.Checked)
            {

                for(int i = 0; i < 500; i++)
                {
                    for(int j = 0; j < 500; j++)
                    {
                        if (matrix[i, j].color == Color.Black) blackBoundaries++;
                    }
                }
                boundariesPercentage = (blackBoundaries / 250000.0f) * 100.0f;

                GBLabel.Text=""+boundariesPercentage;
            }

            ready = false;
            first = true;
            if (SubstructureCheckBox.Checked || DualPhaseCheckBox.Checked) first = false;
            pictureBox1.Image = bitmap;
            pictureBox1.Refresh();
        }

        private void GrowthButton_Click(object sender, EventArgs e)
        {

            //checking checkboxes
            if (!(VonNeumannCheckbox.Checked || MooreCheckbox.Checked || Moore2Checkbox.Checked))
            {
                MessageBox.Show("Please select neighbourhood type.", "Neighbourhood type", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!(AbsorbingCheckbox.Checked || PeriodicCheckbox.Checked))
            {
                MessageBox.Show("Please select boundary conditions type.", "Boundary conditions type", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //inclusions before
            if (BeforeCheckBox.Checked) inclusions();

            //von Neumann and absorbing boundary conditions
            if (VonNeumannCheckbox.Checked && AbsorbingCheckbox.Checked)
            {
                while (!full)
                {
                    full = true;
                    int[] counter = new int[4];
                    Color[] colors = new Color[4];
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            if (first)
                            {
                                // add matrix to matrixI
                                for (int k = 0; k < 500; k++)
                                {
                                    for (int l = 0; l < 500; l++)
                                    {
                                        matrixI[k, l].color = matrix[k, l].color;
                                    }
                                }
                                first = false;
                            }
                            if (matrix[i, j].color == Color.White)
                            {

                                if (i + 1 < 500 && matrix[i + 1, j].color != Color.White && matrix[i + 1, j].color != Color.Black 
                                    && !savedColors.Contains(matrix[i + 1, j].color)
                                    )
                                {
                                    colors[0] = matrix[i + 1, j].color;
                                    counter[0]++;
                                    full = false;
                                }
                                if (i - 1 >= 0 && matrix[i - 1, j].color != Color.White && matrix[i - 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j].color)
                                    )
                                {
                                    if (matrix[i - 1, j].color == colors[0]) counter[0]++;
                                    else
                                    {
                                        colors[1] = matrix[i - 1, j].color;
                                        counter[1]++;
                                    }
                                    full = false;
                                }
                                if (j + 1 < 500 && matrix[i, j + 1].color != Color.White && matrix[i, j + 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i, j + 1].color)
                                    )
                                {
                                    if (matrix[i, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j + 1].color == colors[1]) counter[1]++;
                                    else
                                    {
                                        colors[2] = matrix[i, j + 1].color;
                                        counter[2]++;
                                    }
                                    full = false;
                                }
                                if (j - 1 >= 0 && matrix[i, j - 1].color != Color.White && matrix[i, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i, j - 1].color)
                                    )
                                {
                                    if (matrix[i, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i, j - 1].color == colors[2]) counter[2]++;
                                    else
                                    {
                                        colors[3] = matrix[i, j - 1].color;
                                        counter[3]++;
                                    }
                                    full = false;
                                }

                                //add max color to grain
                                int maxValue = counter.Max();
                                int maxIndex = counter.ToList().IndexOf(maxValue);

                                if (maxIndex == 0 && i + 1 < 500) matrixI[i, j].color = colors[0];
                                if (maxIndex == 1 && i - 1 >= 0) matrixI[i, j].color = colors[1];
                                if (maxIndex == 2 && j + 1 < 500) matrixI[i, j].color =colors[2];
                                if (maxIndex == 3 && j - 1 >= 0) matrixI[i, j].color = colors[3];

                                for(int k = 0; k < 4; k++)
                                {
                                    colors[k] = Color.White;
                                    counter[k] = 0;
                                }
                            }
                        }
                    }

                    // replace matrixes and set bitmap
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                                matrix[i, j].color = matrixI[i, j].color;
                        }
                    }

                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            bitmap.SetPixel(i, j, matrixI[i, j].color);
                        }
                    }

                    pictureBox1.Image = bitmap;
                    pictureBox1.Refresh();
                }
            }
            
            //von Neumann and periodic boundary conditions
            if (VonNeumannCheckbox.Checked && PeriodicCheckbox.Checked)
            {
                while (!full)
                {
                    full = true;
                    int[] counter = new int[4];
                    Color[] colors = new Color[4];
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            if (first)
                            {
                                // add matrix to matrixI
                                for (int k = 0; k < 500; k++)
                                {
                                    for (int l = 0; l < 500; l++)
                                    {
                                        matrixI[k, l].color = matrix[k, l].color;
                                    }
                                }
                                first = false;
                            }
                            if (matrix[i, j].color == Color.White)
                            {

                                if (i + 1 < 500 && matrix[i + 1, j].color != Color.White && matrix[i + 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i + 1, j].color)
                                    )
                                {
                                    colors[0] = matrix[i + 1, j].color;
                                    counter[0]++;
                                    full = false;
                                }
                                if (i + 1 > 499)
                                {
                                    if (matrix[i + 1 - 500, j].color != Color.White && matrix[i + 1 - 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1 - 500, j].color)
                                        )
                                    {
                                        colors[0] = matrix[i + 1-500, j].color;
                                        counter[0]++;
                                        full = false;
                                    }
                                }


                                if (i - 1 >= 0 && matrix[i - 1, j].color != Color.White && matrix[i - 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j].color)
                                    )
                                {
                                    if (matrix[i - 1, j].color == colors[0]) counter[0]++;
                                    else
                                    {
                                        colors[1] = matrix[i - 1, j].color;
                                        counter[1]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 < 0)
                                {
                                    if (matrix[i - 1 + 500, j].color != Color.White && matrix[i - 1 + 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1 + 500, j].color)
                                        )
                                    {
                                        if (matrix[i - 1+500, j].color == colors[0]) counter[0]++;
                                        else
                                        {
                                            colors[1] = matrix[i - 1+500, j].color;
                                            counter[1]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (j + 1 < 500 && matrix[i, j + 1].color != Color.White && matrix[i, j + 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i, j + 1].color)
                                    )
                                {
                                    if (matrix[i, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j + 1].color == colors[1]) counter[1]++;
                                    else
                                    {
                                        colors[2] = matrix[i, j + 1].color;
                                        counter[2]++;
                                    }
                                    full = false;
                                }
                                if (j + 1 > 499)
                                {
                                    if (matrix[i, j + 1 - 500].color != Color.White && matrix[i, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i, j + 1-500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j + 1-500].color == colors[1]) counter[1]++;
                                        else
                                        {
                                            colors[2] = matrix[i, j + 1-500].color;
                                            counter[2]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (j - 1 >= 0 && matrix[i, j - 1].color != Color.White && matrix[i, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i, j - 1].color)
                                    )
                                {
                                    if (matrix[i, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i, j - 1].color == colors[2]) counter[2]++;
                                    else
                                    {
                                        colors[3] = matrix[i, j - 1].color;
                                        counter[3]++;
                                    }
                                    full = false;
                                }

                                if (j - 1 < 0)
                                {
                                    if (matrix[i, j - 1 + 500].color != Color.White && matrix[i, j - 1 + 500].color != Color.Black
                                         && !savedColors.Contains(matrix[i, j - 1 + 500].color)
                                        )
                                    {
                                        if (matrix[i, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else
                                        {
                                            colors[3] = matrix[i, j - 1 + 500].color;
                                            counter[3]++;
                                        }
                                        full = false;
                                    }
                                }
                                //add max color to grain
                                int maxValue = counter.Max();
                                int maxIndex = counter.ToList().IndexOf(maxValue);

                                if (maxIndex == 0 ) matrixI[i, j].color = colors[0];
                                if (maxIndex == 1 ) matrixI[i, j].color = colors[1];
                                if (maxIndex == 2 ) matrixI[i, j].color = colors[2];
                                if (maxIndex == 3 ) matrixI[i, j].color = colors[3];

                                for (int k = 0; k < 4; k++)
                                {
                                    colors[k] = Color.White;
                                    counter[k] = 0;
                                }

                            }
                        }
                    }

                    // replace matrixes and set bitmap
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            matrix[i, j].color = matrixI[i, j].color;
                        }
                    }

                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            bitmap.SetPixel(i, j, matrixI[i, j].color);
                        }
                    }

                    pictureBox1.Image = bitmap;
                    pictureBox1.Refresh();
                }
            }

            //Moore and absorbing boundary conditions
            if (MooreCheckbox.Checked && AbsorbingCheckbox.Checked)
            {
                while (!full)
                {
                    full = true;
                    int[] counter = new int[8];
                    Color[] colors = new Color[8];
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            if (first)
                            {
                                // add matrix to matrixI
                                for (int k = 0; k < 500; k++)
                                {
                                    for (int l = 0; l < 500; l++)
                                    {
                                        matrixI[k, l].color = matrix[k, l].color;
                                    }
                                }
                                first = false;
                            }
                            if (matrix[i, j].color == Color.White)
                            {
                                if (i + 1 < 500 && matrix[i + 1, j].color != Color.White && matrix[i + 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i + 1, j].color)
                                    )
                                {
                                    colors[0] = matrix[i + 1, j].color;
                                    counter[0]++;
                                    full = false;
                                }
                                if (i - 1 >= 0 && matrix[i - 1, j].color != Color.White && matrix[i - 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j].color)
                                    )
                                {
                                    if (matrix[i-1, j].color == colors[0]) counter[0]++;
                                    else
                                    {
                                        colors[1] = matrix[i - 1, j].color;
                                        counter[1]++;
                                    }
                                    full = false;
                                }
                                if (j + 1 < 500 && matrix[i, j + 1].color != Color.White && matrix[i, j + 1].color != Color.Black
                                     && !savedColors.Contains(matrix[i, j + 1].color)
                                    )
                                {
                                    if (matrix[i, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j + 1].color == colors[1]) counter[1]++;
                                    else
                                    {
                                        colors[2] = matrix[i, j + 1].color;
                                        counter[2]++;
                                    }
                                    full = false;
                                }
                                if (j - 1 >= 0 && matrix[i, j - 1].color != Color.White && matrix[i, j - 1].color != Color.Black
                                     && !savedColors.Contains(matrix[i, j - 1].color)
                                    )
                                {
                                    if (matrix[i, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i, j - 1].color == colors[2]) counter[2]++;
                                    else
                                    {
                                        colors[3] = matrix[i, j - 1].color;
                                        counter[3]++;
                                    }
                                    full = false;
                                }
                                if (i + 1 < 500 && j + 1 <500 &&  matrix[i+1, j + 1].color != Color.White && matrix[i + 1, j + 1].color != Color.Black
                                     && !savedColors.Contains(matrix[i + 1, j + 1].color)
                                    )
                                {
                                    if (matrix[i + 1, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i + 1, j + 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i + 1, j + 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i + 1, j + 1].color == colors[3]) counter[3]++;
                                    else
                                    {
                                        colors[4] = matrix[i + 1, j + 1].color;
                                        counter[4]++;
                                    }
                                    full = false;
                                }
                                if (i + 1 < 500 && j - 1 >=0 &&  matrix[i + 1, j - 1].color != Color.White && matrix[i + 1, j - 1].color != Color.Black
                                      && !savedColors.Contains(matrix[i + 1, j - 1].color)
                                    )
                                {
                                    if (matrix[i + 1, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i + 1, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i + 1, j - 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i + 1, j - 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i + 1, j - 1].color == colors[4]) counter[4]++;
                                    else
                                    {
                                        colors[5] = matrix[i + 1, j - 1].color;
                                        counter[5]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 >=0 && j - 1 >= 0 && matrix[i - 1, j - 1].color != Color.White && matrix[i - 1, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j - 1].color)
                                    )
                                {
                                    if (matrix[i - 1, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i - 1, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i - 1, j - 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i - 1, j - 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i - 1, j - 1].color == colors[4]) counter[4]++;
                                    else if (matrix[i - 1, j - 1].color == colors[5]) counter[5]++;
                                    else
                                    {
                                        colors[6] = matrix[i - 1, j - 1].color;
                                        counter[6]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 >= 0 && j + 1<500 && matrix[i - 1, j + 1].color != Color.White && matrix[i - 1, j + 1].color != Color.Black
                                     && !savedColors.Contains(matrix[i - 1, j + 1].color)
                                    )
                                {
                                    if (matrix[i - 1, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i - 1, j + 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i - 1, j + 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i - 1, j + 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i - 1, j + 1].color == colors[4]) counter[4]++;
                                    else if (matrix[i - 1, j + 1].color == colors[5]) counter[5]++;
                                    else if (matrix[i - 1, j + 1].color == colors[6]) counter[6]++;
                                    else
                                    {
                                        colors[7] = matrix[i - 1, j + 1].color;
                                        counter[7]++;
                                    }
                                    full = false;
                                }

                                //add max color to grain
                                int maxValue = counter.Max();
                                int maxIndex = counter.ToList().IndexOf(maxValue);

                                if (maxIndex == 0) matrixI[i, j].color = colors[0];
                                if (maxIndex == 1) matrixI[i, j].color = colors[1];
                                if (maxIndex == 2) matrixI[i, j].color = colors[2];
                                if (maxIndex == 3) matrixI[i, j].color = colors[3];
                                if (maxIndex == 4) matrixI[i, j].color = colors[4];
                                if (maxIndex == 5) matrixI[i, j].color = colors[5];
                                if (maxIndex == 6) matrixI[i, j].color = colors[6];
                                if (maxIndex == 7) matrixI[i, j].color = colors[7];

                                for (int k = 0; k < 8; k++)
                                {
                                    colors[k] = Color.White;
                                    counter[k] = 0;
                                }
                            }
                        }
                    }

                    // replace matrixes and set bitmap
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            matrix[i, j].color = matrixI[i, j].color;
                        }
                    }

                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            bitmap.SetPixel(i, j, matrixI[i, j].color);
                        }
                    }

                    pictureBox1.Image = bitmap;
                    pictureBox1.Refresh();
                }
            }

            //Moore and periodic boundary conditions
            if (MooreCheckbox.Checked && PeriodicCheckbox.Checked)
            {
                while (!full)
                {
                    full = true;
                    int[] counter = new int[8];
                    Color[] colors = new Color[8];
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            if (first)
                            {
                                // add matrix to matrixI
                                for (int k = 0; k < 500; k++)
                                {
                                    for (int l = 0; l < 500; l++)
                                    {
                                        matrixI[k, l].color = matrix[k, l].color;
                                    }
                                }
                                first = false;
                            }
                            if (matrix[i, j].color == Color.White)
                            {
                                
                                if (i + 1 < 500 && matrix[i + 1, j].color != Color.White && matrix[i + 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i + 1, j].color)
                                    )
                                {
                                    colors[0] = matrix[i + 1, j].color;
                                    counter[0]++;
                                    full = false;
                                }
                                if (i + 1 > 499)
                                {
                                    if (matrix[i + 1 - 500, j].color != Color.White && matrix[i + 1 - 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1 - 500, j].color)
                                        )
                                    {
                                        colors[0] = matrix[i + 1-500, j].color;
                                        counter[0]++;
                                        full = false;
                                    }
                                }


                                if (i - 1 >= 0 && matrix[i - 1, j].color != Color.White && matrix[i - 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j].color)
                                    )
                                {
                                    if (matrix[i - 1, j].color == colors[0]) counter[0]++;
                                    else
                                    {
                                        colors[1] = matrix[i - 1, j].color;
                                        counter[1]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 < 0)
                                {
                                    if (matrix[i - 1 + 500, j].color != Color.White && matrix[i - 1 + 500, j].color != Color.Black
                                         && !savedColors.Contains(matrix[i - 1 + 500, j].color)
                                        )
                                    {
                                        if (matrix[i - 1 + 500, j].color == colors[0]) counter[0]++;
                                        else
                                        {
                                            colors[1] = matrix[i - 1 + 500, j].color;
                                            counter[1]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (j + 1 < 500 && matrix[i, j + 1].color != Color.White && matrix[i, j + 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i, j + 1].color)
                                    )
                                {
                                    if (matrix[i, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j + 1].color == colors[1]) counter[1]++;
                                    else
                                    {
                                        colors[2] = matrix[i, j + 1].color;
                                        counter[2]++;
                                    }
                                    full = false;
                                }
                                if (j + 1 > 499)
                                {
                                    if (matrix[i, j + 1 - 500].color != Color.White && matrix[i, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i, j + 1 - 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[1]) counter[1]++;
                                        else
                                        {
                                            colors[2] = matrix[i, j + 1 - 500].color;
                                            counter[2]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (j - 1 >= 0 && matrix[i, j - 1].color != Color.White && matrix[i, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i, j - 1].color)
                                    )
                                {
                                    if (matrix[i, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i, j - 1].color == colors[2]) counter[2]++;
                                    else
                                    {
                                        colors[3] = matrix[i, j - 1].color;
                                        counter[3]++;
                                    }
                                    full = false;
                                }
                                if (j - 1 < 0)
                                {
                                    if (matrix[i, j - 1 + 500].color != Color.White && matrix[i, j - 1 + 500].color != Color.Black
                                         && !savedColors.Contains(matrix[i, j - 1 + 500].color)
                                         )
                                    {
                                        if (matrix[i, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else
                                        {
                                            colors[3] = matrix[i, j - 1 + 500].color;
                                            counter[3]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (i + 1 < 500 && j + 1 < 500 && matrix[i + 1, j + 1].color != Color.White && matrix[i + 1, j + 1].color != Color.Black
                                     && !savedColors.Contains(matrix[i + 1, j + 1].color)
                                    )
                                {
                                    if (matrix[i + 1, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i + 1, j + 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i + 1, j + 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i + 1, j + 1].color == colors[3]) counter[3]++;
                                    else
                                    {
                                        colors[4] = matrix[i + 1, j + 1].color;
                                        counter[4]++;
                                    }
                                    full = false;
                                }
                                if (i + 1 > 499 && j + 1 > 499)
                                {
                                    if (matrix[i + 1-500, j + 1-500].color != Color.White && matrix[i + 1 - 500, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1 - 500, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i + 1 - 500, j + 1 - 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i + 1 - 500, j + 1 - 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i + 1 - 500, j + 1 - 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i + 1 - 500, j + 1 - 500].color == colors[3]) counter[3]++;
                                        else
                                        {
                                            colors[4] = matrix[i + 1 - 500, j + 1 - 500].color;
                                            counter[4]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (i + 1 > 499)
                                {
                                    if (matrix[i + 1 - 500, j].color != Color.White && matrix[i + 1 - 500, j].color != Color.Black
                                         && !savedColors.Contains(matrix[i + 1 - 500, j].color)
                                        )
                                    {
                                        if (matrix[i + 1 - 500, j].color == colors[0]) counter[0]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[1]) counter[1]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[2]) counter[2]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[3]) counter[3]++;
                                        else
                                        {
                                            colors[4] = matrix[i + 1 - 500, j].color;
                                            counter[4]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (j + 1 > 499)
                                {
                                    if (matrix[i, j + 1 - 500].color != Color.White && matrix[i, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i, j + 1 - 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[3]) counter[3]++;
                                        else
                                        {
                                            colors[4] = matrix[i, j + 1 - 500].color;
                                            counter[4]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (i + 1 < 500 && j - 1 >= 0 && matrix[i + 1, j - 1].color != Color.White && matrix[i + 1, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i + 1, j - 1].color)
                                    )
                                {
                                    if (matrix[i + 1, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i + 1, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i + 1, j - 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i + 1, j - 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i + 1, j - 1].color == colors[4]) counter[4]++;
                                    else
                                    {
                                        colors[5] = matrix[i + 1, j - 1].color;
                                        counter[5]++;
                                    }
                                    full = false;
                                }
                                if (i + 1 > 499 && j - 1 < 0)
                                {
                                    if (matrix[i + 1 - 500, j - 1 + 500].color != Color.White && matrix[i + 1 - 500, j - 1 + 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1 - 500, j - 1 + 500].color)
                                        )
                                    {
                                        if (matrix[i + 1 - 500, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i + 1 - 500, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i + 1 - 500, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i + 1 - 500, j - 1 + 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i + 1 - 500, j - 1 + 500].color == colors[4]) counter[4]++;
                                        else
                                        {
                                            colors[5] = matrix[i + 1 - 500, j - 1 + 500].color;
                                            counter[5]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (i + 1 > 499)
                                {
                                    if (matrix[i + 1 - 500, j].color != Color.White && matrix[i + 1 - 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1 - 500, j].color)
                                        )
                                    {
                                        if (matrix[i + 1 - 500, j].color == colors[0]) counter[0]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[1]) counter[1]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[2]) counter[2]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[3]) counter[3]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[4]) counter[4]++;
                                        else
                                        {
                                            colors[5] = matrix[i + 1 - 500, j].color;
                                            counter[5]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (j - 1 < 0)
                                {
                                    if (matrix[i, j - 1 + 500].color != Color.White && matrix[i, j - 1 + 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j - 1 + 500].color)
                                        )
                                    {
                                        if (matrix[i, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[4]) counter[4]++;
                                        else
                                        {
                                            colors[5] = matrix[i, j - 1 + 500].color;
                                            counter[5]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (i - 1 >= 0 && j - 1 >= 0 && matrix[i - 1, j - 1].color != Color.White && matrix[i - 1, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j - 1].color)
                                    )
                                {
                                    if (matrix[i - 1, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i - 1, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i - 1, j - 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i - 1, j - 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i - 1, j - 1].color == colors[4]) counter[4]++;
                                    else if (matrix[i - 1, j - 1].color == colors[5]) counter[5]++;
                                    else
                                    {
                                        colors[6] = matrix[i - 1, j - 1].color;
                                        counter[6]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 < 0 && j - 1 < 0)
                                {
                                    if (matrix[i - 1 + 500, j - 1 + 500].color != Color.White && matrix[i - 1 + 500, j - 1 + 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1 + 500, j - 1 + 500].color)
                                        )
                                    {
                                        if (matrix[i - 1 + 500, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i - 1 + 500, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i - 1 + 500, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i - 1 + 500, j - 1 + 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i - 1 + 500, j - 1 + 500].color == colors[4]) counter[4]++;
                                        else if (matrix[i - 1 + 500, j - 1 + 500].color == colors[5]) counter[5]++;
                                        else
                                        {
                                            colors[6] = matrix[i - 1 + 500, j - 1 + 500].color;
                                            counter[6]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (i - 1 < 0)
                                {
                                    if (matrix[i - 1 + 500, j].color != Color.White && matrix[i - 1 + 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1 + 500, j].color)
                                        )
                                    {
                                        if (matrix[i - 1 + 500, j].color == colors[0]) counter[0]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[1]) counter[1]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[2]) counter[2]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[3]) counter[3]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[4]) counter[4]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[5]) counter[5]++;
                                        else
                                        {
                                            colors[6] = matrix[i - 1 + 500, j].color;
                                            counter[6]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (j - 1 < 0)
                                {
                                    if (matrix[i, j - 1 + 500].color != Color.White && matrix[i, j - 1 + 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j - 1 + 500].color)
                                        )
                                    {
                                        if (matrix[i, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[4]) counter[4]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[5]) counter[5]++;
                                        else
                                        {
                                            colors[6] = matrix[i, j - 1 + 500].color;
                                            counter[6]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (i - 1 >= 0 && j + 1 < 500 && matrix[i - 1, j + 1].color != Color.White && matrix[i - 1, j + 1].color != Color.Black
                                     && !savedColors.Contains(matrix[i - 1, j + 1].color)
                                    )
                                {
                                    if (matrix[i - 1, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i - 1, j + 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i - 1, j + 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i - 1, j + 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i - 1, j + 1].color == colors[4]) counter[4]++;
                                    else if (matrix[i - 1, j + 1].color == colors[5]) counter[5]++;
                                    else if (matrix[i - 1, j + 1].color == colors[6]) counter[6]++;
                                    else
                                    {
                                        colors[7] = matrix[i - 1, j + 1].color;
                                        counter[7]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 < 0 && j + 1 >499 )
                                {
                                    if (matrix[i - 1 + 500, j + 1 - 500].color != Color.White && matrix[i - 1 + 500, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1 + 500, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i - 1 + 500, j + 1 - 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[4]) counter[4]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[5]) counter[5]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[6]) counter[6]++;
                                        else
                                        {
                                            colors[7] = matrix[i - 1 + 500, j + 1 - 500].color;
                                            counter[7]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (i - 1 < 0)
                                {
                                    if (matrix[i - 1 + 500, j].color != Color.White && matrix[i - 1 + 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1 + 500, j].color)
                                        )
                                    {
                                        if (matrix[i - 1 + 500, j].color == colors[0]) counter[0]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[1]) counter[1]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[2]) counter[2]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[3]) counter[3]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[4]) counter[4]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[5]) counter[5]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[6]) counter[6]++;
                                        else
                                        {
                                            colors[7] = matrix[i - 1 + 500, j].color;
                                            counter[7]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (j + 1 > 499)
                                {
                                    if (matrix[i, j + 1 - 500].color != Color.White && matrix[i, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i, j + 1 - 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[4]) counter[4]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[5]) counter[5]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[6]) counter[6]++;
                                        else
                                        {
                                            colors[7] = matrix[i, j + 1 - 500].color;
                                            counter[7]++;
                                        }
                                        full = false;
                                    }
                                }

                                //add max color to grain
                                int maxValue = counter.Max();
                                int maxIndex = counter.ToList().IndexOf(maxValue);

                                if (maxIndex == 0) matrixI[i, j].color = colors[0];
                                if (maxIndex == 1) matrixI[i, j].color = colors[1];
                                if (maxIndex == 2) matrixI[i, j].color = colors[2];
                                if (maxIndex == 3) matrixI[i, j].color = colors[3];
                                if (maxIndex == 4) matrixI[i, j].color = colors[4];
                                if (maxIndex == 5) matrixI[i, j].color = colors[5];
                                if (maxIndex == 6) matrixI[i, j].color = colors[6];
                                if (maxIndex == 7) matrixI[i, j].color = colors[7];

                                for (int k = 0; k < 8; k++)
                                {
                                    colors[k] = Color.White;
                                    counter[k] = 0;
                                }
                            }
                        }
                    }

                    // replace matrixes and set bitmap
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            matrix[i, j].color = matrixI[i, j].color;
                        }
                    }

                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            bitmap.SetPixel(i, j, matrixI[i, j].color);
                        }
                    }

                    pictureBox1.Image = bitmap;
                    pictureBox1.Refresh();
                }
            }

            //Moore 2 and absorbing boundary conditions
            if (Moore2Checkbox.Checked && AbsorbingCheckbox.Checked)
            {
                bool done;
                int[] counter = new int[8];
                int[] counter2 = new int[4];
                int[] counter3 = new int[4];
                Color[] colors = new Color[8];
                Color[] colors2 = new Color[4];
                Color[] colors3 = new Color[4];

                for (int k = 0; k < 8; k++)
                {
                    colors[k] = Color.White;
                    counter[k] = 0;
                }
                for (int k = 0; k < 4; k++)
                {
                    colors2[k] = Color.White;
                    colors3[k] = Color.White;
                    counter2[k] = 0;
                    counter3[k] = 0;
                }

                while (!full)
                {
                    full = true;
                    
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            done = false;
                            if (first)
                            {
                                // add matrix to matrixI
                                for (int k = 0; k < 500; k++)
                                {
                                    for (int l = 0; l < 500; l++)
                                    {
                                        matrixI[k, l].color = matrix[k, l].color;
                                    }
                                }
                                first = false;
                            }
                            if (matrix[i, j].color == Color.White)
                            {
                                if (i + 1 < 500 && matrix[i + 1, j].color != Color.White && matrix[i + 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i + 1, j].color)
                                    )
                                {
                                    colors[0] = matrix[i + 1, j].color;
                                    counter[0]++;
                                    full = false;
                                }
                                if (i - 1 >= 0 && matrix[i - 1, j].color != Color.White && matrix[i - 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j].color)
                                    )
                                {
                                    if (matrix[i - 1, j].color == colors[0]) counter[0]++;
                                    else
                                    {
                                        colors[1] = matrix[i - 1, j].color;
                                        counter[1]++;
                                    }
                                    full = false;
                                }
                                if (j + 1 < 500 && matrix[i, j + 1].color != Color.White && matrix[i, j + 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i, j + 1].color)
                                    )
                                {
                                    if (matrix[i, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j + 1].color == colors[1]) counter[1]++;
                                    else
                                    {
                                        colors[2] = matrix[i, j + 1].color;
                                        counter[2]++;
                                    }
                                    full = false;
                                }
                                if (j - 1 >= 0 && matrix[i, j - 1].color != Color.White && matrix[i, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i, j - 1].color)
                                    )
                                {
                                    if (matrix[i, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i, j - 1].color == colors[2]) counter[2]++;
                                    else
                                    {
                                        colors[3] = matrix[i, j - 1].color;
                                        counter[3]++;
                                    }
                                    full = false;
                                }
                                if (i + 1 < 500 && j + 1 < 500 && matrix[i + 1, j + 1].color != Color.White && matrix[i + 1, j + 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i + 1, j + 1].color)
                                    )
                                {
                                    if (matrix[i + 1, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i + 1, j + 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i + 1, j + 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i + 1, j + 1].color == colors[3]) counter[3]++;
                                    else
                                    {
                                        colors[4] = matrix[i + 1, j + 1].color;
                                        counter[4]++;
                                    }
                                    full = false;
                                }
                                if (i + 1 < 500 && j - 1 >= 0 && matrix[i + 1, j - 1].color != Color.White && matrix[i + 1, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i + 1, j - 1].color)
                                    )
                                {
                                    if (matrix[i + 1, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i + 1, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i + 1, j - 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i + 1, j - 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i + 1, j - 1].color == colors[4]) counter[4]++;
                                    else
                                    {
                                        colors[5] = matrix[i + 1, j - 1].color;
                                        counter[5]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 >= 0 && j - 1 >= 0 && matrix[i - 1, j - 1].color != Color.White && matrix[i - 1, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j - 1].color)
                                    )
                                {
                                    if (matrix[i - 1, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i - 1, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i - 1, j - 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i - 1, j - 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i - 1, j - 1].color == colors[4]) counter[4]++;
                                    else if (matrix[i - 1, j - 1].color == colors[5]) counter[5]++;
                                    else
                                    {
                                        colors[6] = matrix[i - 1, j - 1].color;
                                        counter[6]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 >= 0 && j + 1 < 500 && matrix[i - 1, j + 1].color != Color.White && matrix[i - 1, j + 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j + 1].color)
                                    )
                                {
                                    if (matrix[i - 1, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i - 1, j + 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i - 1, j + 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i - 1, j + 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i - 1, j + 1].color == colors[4]) counter[4]++;
                                    else if (matrix[i - 1, j + 1].color == colors[5]) counter[5]++;
                                    else if (matrix[i - 1, j + 1].color == colors[6]) counter[6]++;
                                    else
                                    {
                                        colors[7] = matrix[i - 1, j + 1].color;
                                        counter[7]++;
                                    }
                                    full = false;
                                }

                                //add max color to grain

                                int maxValue = counter.Max();
                                if (maxValue >= 5)
                                {
                                    int maxIndex = counter.ToList().IndexOf(maxValue);
                             
                                    if (maxIndex == 0) matrixI[i, j].color = colors[0];
                                    if (maxIndex == 1) matrixI[i, j].color = colors[1];
                                    if (maxIndex == 2) matrixI[i, j].color = colors[2];
                                    if (maxIndex == 3) matrixI[i, j].color = colors[3];
                                    if (maxIndex == 4) matrixI[i, j].color = colors[4];
                                    if (maxIndex == 5) matrixI[i, j].color = colors[5];
                                    if (maxIndex == 6) matrixI[i, j].color = colors[6];
                                    if (maxIndex == 7) matrixI[i, j].color = colors[7];

                                    done = true;
                                }

                                if (!done)
                                {
                                    if (i + 1 < 500 && matrix[i + 1, j].color != Color.White && matrix[i + 1, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1, j].color)
                                        )
                                    {
                                        colors2[0] = matrix[i + 1, j].color;
                                        counter2[0]++;
                                        full = false;
                                    }
                                    if (i - 1 >= 0 && matrix[i - 1, j].color != Color.White && matrix[i - 1, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1, j].color)
                                        )
                                    {
                                        if (matrix[i - 1, j].color == colors2[0]) counter2[0]++;
                                        else
                                        {
                                            colors2[1] = matrix[i - 1, j].color;
                                            counter2[1]++;
                                        }
                                        full = false;
                                    }
                                    if (j + 1 < 500 && matrix[i, j + 1].color != Color.White && matrix[i, j + 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j + 1].color)
                                        )
                                    {
                                        if (matrix[i, j + 1].color == colors2[0]) counter2[0]++;
                                        else if (matrix[i, j + 1].color == colors2[1]) counter2[1]++;
                                        else
                                        {
                                            colors2[2] = matrix[i, j + 1].color;
                                            counter2[2]++;
                                        }
                                        full = false;
                                    }
                                    if (j - 1 >= 0 && matrix[i, j - 1].color != Color.White && matrix[i, j - 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j - 1].color)
                                        )
                                    {
                                        if (matrix[i, j - 1].color == colors2[0]) counter2[0]++;
                                        else if (matrix[i, j - 1].color == colors2[1]) counter2[1]++;
                                        else if (matrix[i, j - 1].color == colors2[2]) counter2[2]++;
                                        else
                                        {
                                            colors2[3] = matrix[i, j - 1].color;
                                            counter2[3]++;
                                        }
                                        full = false;
                                    }
                                
                                    maxValue = counter2.Max();
                                    if (maxValue >= 3)
                                    {
                                        int maxIndex = counter2.ToList().IndexOf(maxValue);

                                        if (maxIndex == 0) matrixI[i, j].color = colors2[0];
                                        if (maxIndex == 1) matrixI[i, j].color = colors2[1];
                                        if (maxIndex == 2) matrixI[i, j].color = colors2[2];
                                        if (maxIndex == 3) matrixI[i, j].color = colors2[3];

                                        done = true;
                                    }
                                }

                                if (!done)
                                {
                                    if (i + 1 < 500 && j + 1 < 500 && matrix[i + 1, j + 1].color != Color.White && matrix[i + 1, j + 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1, j + 1].color)
                                        )
                                    {
                                        colors[0] = matrix[i + 1, j + 1].color;
                                        counter[0]++;   
                                        full = false;
                                    }
                                    if (i + 1 < 500 && j - 1 >= 0 && matrix[i + 1, j - 1].color != Color.White && matrix[i + 1, j - 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1, j - 1].color)
                                        )
                                    {
                                        if (matrix[i + 1, j - 1].color == colors3[0]) counter3[0]++;
                                        else
                                        {
                                            colors3[1] = matrix[i + 1, j - 1].color;
                                            counter3[1]++;
                                        }
                                        full = false;
                                    }
                                    if (i - 1 >= 0 && j - 1 >= 0 && matrix[i - 1, j - 1].color != Color.White && matrix[i - 1, j - 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1, j - 1].color)
                                        )
                                    {
                                        if (matrix[i - 1, j - 1].color == colors3[0]) counter3[0]++;
                                        else if (matrix[i - 1, j - 1].color == colors3[1]) counter3[1]++;
                                        else
                                        {
                                            colors3[2] = matrix[i - 1, j - 1].color;
                                            counter3[2]++;
                                        }
                                        full = false;
                                    }
                                    if (i - 1 >= 0 && j + 1 < 500 && matrix[i - 1, j + 1].color != Color.White && matrix[i - 1, j + 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1, j + 1].color)
                                        )
                                    {
                                        if (matrix[i - 1, j + 1].color == colors3[0]) counter3[0]++;
                                        else if (matrix[i - 1, j + 1].color == colors3[1]) counter3[1]++;
                                        else if (matrix[i - 1, j + 1].color == colors3[2]) counter3[2]++;
                                        else
                                        {
                                            colors3[3] = matrix[i - 1, j + 1].color;
                                            counter3[3]++;
                                        }
                                        full = false;
                                    }

                                    maxValue = counter3.Max();
                                    if (maxValue >= 3)
                                    {
                                        int maxIndex = counter3.ToList().IndexOf(maxValue);

                                        if (maxIndex == 0) matrixI[i, j].color = colors3[0];
                                        if (maxIndex == 1) matrixI[i, j].color = colors3[1];
                                        if (maxIndex == 2) matrixI[i, j].color = colors3[2];
                                        if (maxIndex == 3) matrixI[i, j].color = colors3[3];

                                        done = true;
                                    }

                                }

                                if (!done)
                                {

                                    //checking if percentage is ok
                                    if (!Int32.TryParse(percentageTextBox.Text, out int percentage))
                                    {
                                        MessageBox.Show("Bad value. Please fix it.", "Percentage", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }


                                    
                                    int chance = rnd.Next(0, 100);

                                    if (chance <= percentage)
                                    {
                                        maxValue = counter.Max();
                                        int maxIndex = counter.ToList().IndexOf(maxValue);

                                        if (maxIndex == 0) matrixI[i, j].color = colors[0];
                                        if (maxIndex == 1) matrixI[i, j].color = colors[1];
                                        if (maxIndex == 2) matrixI[i, j].color = colors[2];
                                        if (maxIndex == 3) matrixI[i, j].color = colors[3];
                                        if (maxIndex == 4) matrixI[i, j].color = colors[4];
                                        if (maxIndex == 5) matrixI[i, j].color = colors[5];
                                        if (maxIndex == 6) matrixI[i, j].color = colors[6];
                                        if (maxIndex == 7) matrixI[i, j].color = colors[7];

                                        done = true;
                                    }

                                }

                                for (int k = 0; k < 8; k++)
                                {
                                    colors[k] = Color.White;
                                    counter[k] = 0;
                                }
                                for (int k = 0; k < 4; k++)
                                {
                                    counter2[k] = 0;
                                    counter3[k] = 0;
                                }

                            }
                        }
                    }

                    // replace matrixes and set bitmap
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            matrix[i, j].color = matrixI[i, j].color;
                        }
                    }

                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            bitmap.SetPixel(i, j, matrixI[i, j].color);
                        }
                    }

                    pictureBox1.Image = bitmap;
                    pictureBox1.Refresh();
                }
            }

            //Moore 2 and periodic boundary conditions
            if (Moore2Checkbox.Checked && PeriodicCheckbox.Checked)
            {
                bool done;
                int[] counter = new int[8];
                int[] counter2 = new int[4];
                int[] counter3 = new int[4];
                Color[] colors = new Color[8];
                Color[] colors2 = new Color[4];
                Color[] colors3 = new Color[4];

                for (int k = 0; k < 8; k++)
                {
                    colors[k] = Color.White;
                    counter[k] = 0;
                }
                for (int k = 0; k < 4; k++)
                {
                    colors2[k] = Color.White;
                    colors3[k] = Color.White;
                    counter2[k] = 0;
                    counter3[k] = 0;
                }

                while (!full)
                {
                    full = true;
                    
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            done = false;
                            if (first)
                            {
                                // add matrix to matrixI
                                for (int k = 0; k < 500; k++)
                                {
                                    for (int l = 0; l < 500; l++)
                                    {
                                        matrixI[k, l].color = matrix[k, l].color;
                                    }
                                }
                                first = false;
                            }
                            if (matrix[i, j].color == Color.White)
                            {

                                if (i + 1 < 500 && matrix[i + 1, j].color != Color.White && matrix[i + 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i + 1, j].color)
                                    )
                                {
                                    colors[0] = matrix[i + 1, j].color;
                                    counter[0]++;
                                    full = false;
                                }
                                if (i + 1 > 499)
                                {
                                    if (matrix[i + 1 - 500, j].color != Color.White && matrix[i + 1 - 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1 - 500, j].color)
                                        )
                                    {
                                        colors[0] = matrix[i + 1 - 500, j].color;
                                        counter[0]++;
                                        full = false;
                                    }
                                }


                                if (i - 1 >= 0 && matrix[i - 1, j].color != Color.White && matrix[i - 1, j].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j].color)
                                    )
                                {
                                    if (matrix[i - 1, j].color == colors[0]) counter[0]++;
                                    else
                                    {
                                        colors[1] = matrix[i - 1, j].color;
                                        counter[1]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 < 0)
                                {
                                    if (matrix[i - 1 + 500, j].color != Color.White && matrix[i - 1 + 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1 + 500, j].color)
                                        )
                                    {
                                        if (matrix[i - 1 + 500, j].color == colors[0]) counter[0]++;
                                        else
                                        {
                                            colors[1] = matrix[i - 1 + 500, j].color;
                                            counter[1]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (j + 1 < 500 && matrix[i, j + 1].color != Color.White && matrix[i, j + 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i, j + 1].color)
                                    )
                                {
                                    if (matrix[i, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j + 1].color == colors[1]) counter[1]++;
                                    else
                                    {
                                        colors[2] = matrix[i, j + 1].color;
                                        counter[2]++;
                                    }
                                    full = false;
                                }
                                if (j + 1 > 499)
                                {
                                    if (matrix[i, j + 1 - 500].color != Color.White && matrix[i, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i, j + 1 - 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[1]) counter[1]++;
                                        else
                                        {
                                            colors[2] = matrix[i, j + 1 - 500].color;
                                            counter[2]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (j - 1 >= 0 && matrix[i, j - 1].color != Color.White && matrix[i, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i, j - 1].color)
                                    )
                                {
                                    if (matrix[i, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i, j - 1].color == colors[2]) counter[2]++;
                                    else
                                    {
                                        colors[3] = matrix[i, j - 1].color;
                                        counter[3]++;
                                    }
                                    full = false;
                                }
                                if (j - 1 < 0)
                                {
                                    if (matrix[i, j - 1 + 500].color != Color.White && matrix[i, j - 1 + 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j - 1 + 500].color)
                                        )
                                    {
                                        if (matrix[i, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else
                                        {
                                            colors[3] = matrix[i, j - 1 + 500].color;
                                            counter[3]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (i + 1 < 500 && j + 1 < 500 && matrix[i + 1, j + 1].color != Color.White && matrix[i + 1, j + 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i + 1, j + 1].color)
                                    )
                                {
                                    if (matrix[i + 1, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i + 1, j + 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i + 1, j + 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i + 1, j + 1].color == colors[3]) counter[3]++;
                                    else
                                    {
                                        colors[4] = matrix[i + 1, j + 1].color;
                                        counter[4]++;
                                    }
                                    full = false;
                                }
                                if (i + 1 > 499 && j + 1 > 499)
                                {
                                    if (matrix[i + 1 - 500, j + 1 - 500].color != Color.White && matrix[i + 1 - 500, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1 - 500, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i + 1 - 500, j + 1 - 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i + 1 - 500, j + 1 - 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i + 1 - 500, j + 1 - 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i + 1 - 500, j + 1 - 500].color == colors[3]) counter[3]++;
                                        else
                                        {
                                            colors[4] = matrix[i + 1 - 500, j + 1 - 500].color;
                                            counter[4]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (i + 1 > 499)
                                {
                                    if (matrix[i + 1 - 500, j].color != Color.White && matrix[i + 1 - 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1 - 500, j].color)
                                        )
                                    {
                                        if (matrix[i + 1 - 500, j].color == colors[0]) counter[0]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[1]) counter[1]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[2]) counter[2]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[3]) counter[3]++;
                                        else
                                        {
                                            colors[4] = matrix[i + 1 - 500, j].color;
                                            counter[4]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (j + 1 > 499)
                                {
                                    if (matrix[i, j + 1 - 500].color != Color.White && matrix[i, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i, j + 1 - 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[3]) counter[3]++;
                                        else
                                        {
                                            colors[4] = matrix[i, j + 1 - 500].color;
                                            counter[4]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (i + 1 < 500 && j - 1 >= 0 && matrix[i + 1, j - 1].color != Color.White && matrix[i + 1, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i + 1, j - 1].color)
                                    )
                                {
                                    if (matrix[i + 1, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i + 1, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i + 1, j - 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i + 1, j - 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i + 1, j - 1].color == colors[4]) counter[4]++;
                                    else
                                    {
                                        colors[5] = matrix[i + 1, j - 1].color;
                                        counter[5]++;
                                    }
                                    full = false;
                                }
                                if (i + 1 > 499 && j - 1 < 0)
                                {
                                    if (matrix[i + 1 - 500, j - 1 + 500].color != Color.White && matrix[i + 1 - 500, j - 1 + 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1 - 500, j - 1 + 500].color)
                                        )
                                    {
                                        if (matrix[i + 1 - 500, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i + 1 - 500, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i + 1 - 500, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i + 1 - 500, j - 1 + 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i + 1 - 500, j - 1 + 500].color == colors[4]) counter[4]++;
                                        else
                                        {
                                            colors[5] = matrix[i + 1 - 500, j - 1 + 500].color;
                                            counter[5]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (i + 1 > 499)
                                {
                                    if (matrix[i + 1 - 500, j].color != Color.White && matrix[i + 1 - 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1 - 500, j].color)
                                        )
                                    {
                                        if (matrix[i + 1 - 500, j].color == colors[0]) counter[0]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[1]) counter[1]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[2]) counter[2]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[3]) counter[3]++;
                                        else if (matrix[i + 1 - 500, j].color == colors[4]) counter[4]++;
                                        else
                                        {
                                            colors[5] = matrix[i + 1 - 500, j].color;
                                            counter[5]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (j - 1 < 0)
                                {
                                    if (matrix[i, j - 1 + 500].color != Color.White && matrix[i, j - 1 + 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j - 1 + 500].color)
                                        )
                                    {
                                        if (matrix[i, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[4]) counter[4]++;
                                        else
                                        {
                                            colors[5] = matrix[i, j - 1 + 500].color;
                                            counter[5]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (i - 1 >= 0 && j - 1 >= 0 && matrix[i - 1, j - 1].color != Color.White && matrix[i - 1, j - 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j - 1].color)
                                    )
                                {
                                    if (matrix[i - 1, j - 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i - 1, j - 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i - 1, j - 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i - 1, j - 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i - 1, j - 1].color == colors[4]) counter[4]++;
                                    else if (matrix[i - 1, j - 1].color == colors[5]) counter[5]++;
                                    else
                                    {
                                        colors[6] = matrix[i - 1, j - 1].color;
                                        counter[6]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 < 0 && j - 1 < 0)
                                {
                                    if (matrix[i - 1 + 500, j - 1 + 500].color != Color.White && matrix[i - 1 + 500, j - 1 + 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1 + 500, j - 1 + 500].color)
                                        )
                                    {
                                        if (matrix[i - 1 + 500, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i - 1 + 500, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i - 1 + 500, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i - 1 + 500, j - 1 + 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i - 1 + 500, j - 1 + 500].color == colors[4]) counter[4]++;
                                        else if (matrix[i - 1 + 500, j - 1 + 500].color == colors[5]) counter[5]++;
                                        else
                                        {
                                            colors[6] = matrix[i - 1 + 500, j - 1 + 500].color;
                                            counter[6]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (i - 1 < 0)
                                {
                                    if (matrix[i - 1 + 500, j].color != Color.White && matrix[i - 1 + 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1 + 500, j].color)
                                        )
                                    {
                                        if (matrix[i - 1 + 500, j].color == colors[0]) counter[0]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[1]) counter[1]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[2]) counter[2]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[3]) counter[3]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[4]) counter[4]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[5]) counter[5]++;
                                        else
                                        {
                                            colors[6] = matrix[i - 1 + 500, j].color;
                                            counter[6]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (j - 1 < 0)
                                {
                                    if (matrix[i, j - 1 + 500].color != Color.White && matrix[i, j - 1 + 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j - 1 + 500].color)
                                        )
                                    {
                                        if (matrix[i, j - 1 + 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[4]) counter[4]++;
                                        else if (matrix[i, j - 1 + 500].color == colors[5]) counter[5]++;
                                        else
                                        {
                                            colors[6] = matrix[i, j - 1 + 500].color;
                                            counter[6]++;
                                        }
                                        full = false;
                                    }
                                }


                                if (i - 1 >= 0 && j + 1 < 500 && matrix[i - 1, j + 1].color != Color.White && matrix[i - 1, j + 1].color != Color.Black
                                    && !savedColors.Contains(matrix[i - 1, j + 1].color)
                                    )
                                {
                                    if (matrix[i - 1, j + 1].color == colors[0]) counter[0]++;
                                    else if (matrix[i - 1, j + 1].color == colors[1]) counter[1]++;
                                    else if (matrix[i - 1, j + 1].color == colors[2]) counter[2]++;
                                    else if (matrix[i - 1, j + 1].color == colors[3]) counter[3]++;
                                    else if (matrix[i - 1, j + 1].color == colors[4]) counter[4]++;
                                    else if (matrix[i - 1, j + 1].color == colors[5]) counter[5]++;
                                    else if (matrix[i - 1, j + 1].color == colors[6]) counter[6]++;
                                    else
                                    {
                                        colors[7] = matrix[i - 1, j + 1].color;
                                        counter[7]++;
                                    }
                                    full = false;
                                }
                                if (i - 1 < 0 && j + 1 > 499)
                                {
                                    if (matrix[i - 1 + 500, j + 1 - 500].color != Color.White && matrix[i - 1 + 500, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1 + 500, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i - 1 + 500, j + 1 - 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[4]) counter[4]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[5]) counter[5]++;
                                        else if (matrix[i - 1 + 500, j + 1 - 500].color == colors[6]) counter[6]++;
                                        else
                                        {
                                            colors[7] = matrix[i - 1 + 500, j + 1 - 500].color;
                                            counter[7]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (i - 1 < 0)
                                {
                                    if (matrix[i - 1 + 500, j].color != Color.White && matrix[i - 1 + 500, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1 + 500, j].color)
                                        )
                                    {
                                        if (matrix[i - 1 + 500, j].color == colors[0]) counter[0]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[1]) counter[1]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[2]) counter[2]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[3]) counter[3]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[4]) counter[4]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[5]) counter[5]++;
                                        else if (matrix[i - 1 + 500, j].color == colors[6]) counter[6]++;
                                        else
                                        {
                                            colors[7] = matrix[i - 1 + 500, j].color;
                                            counter[7]++;
                                        }
                                        full = false;
                                    }
                                }
                                else if (j + 1 > 499)
                                {
                                    if (matrix[i, j + 1 - 500].color != Color.White && matrix[i, j + 1 - 500].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j + 1 - 500].color)
                                        )
                                    {
                                        if (matrix[i, j + 1 - 500].color == colors[0]) counter[0]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[1]) counter[1]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[2]) counter[2]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[3]) counter[3]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[4]) counter[4]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[5]) counter[5]++;
                                        else if (matrix[i, j + 1 - 500].color == colors[6]) counter[6]++;
                                        else
                                        {
                                            colors[7] = matrix[i, j + 1 - 500].color;
                                            counter[7]++;
                                        }
                                        full = false;
                                    }
                                }

                                //add max color to grain
                                int maxValue = counter.Max();
                                if (maxValue >= 5)
                                {
                                    int maxIndex = counter.ToList().IndexOf(maxValue);

                                    if (maxIndex == 0) matrixI[i, j].color = colors[0];
                                    if (maxIndex == 1) matrixI[i, j].color = colors[1];
                                    if (maxIndex == 2) matrixI[i, j].color = colors[2];
                                    if (maxIndex == 3) matrixI[i, j].color = colors[3];
                                    if (maxIndex == 4) matrixI[i, j].color = colors[4];
                                    if (maxIndex == 5) matrixI[i, j].color = colors[5];
                                    if (maxIndex == 6) matrixI[i, j].color = colors[6];
                                    if (maxIndex == 7) matrixI[i, j].color = colors[7];

                                    done = true;
                                }

                                if (!done)
                                {

                                    if (i + 1 < 500 && matrix[i + 1, j].color != Color.White && matrix[i + 1, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1, j].color)
                                        )
                                    {
                                        colors2[0] = matrix[i + 1, j].color;
                                        counter2[0]++;
                                        full = false;
                                    }
                                    if (i + 1 > 499)
                                    {
                                        if (matrix[i + 1 - 500, j].color != Color.White && matrix[i + 1 - 500, j].color != Color.Black
                                            && !savedColors.Contains(matrix[i + 1 - 500, j].color)
                                            )
                                        {
                                            colors2[0] = matrix[i + 1 - 500, j].color;
                                            counter2[0]++;
                                            full = false;
                                        }
                                    }


                                    if (i - 1 >= 0 && matrix[i - 1, j].color != Color.White && matrix[i - 1, j].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1, j].color)
                                        )
                                    {
                                        if (matrix[i - 1, j].color == colors2[0]) counter2[0]++;
                                        else
                                        {
                                            colors2[1] = matrix[i - 1, j].color;
                                            counter2[1]++;
                                        }
                                        full = false;
                                    }
                                    if (i - 1 < 0)
                                    {
                                        if (matrix[i - 1 + 500, j].color != Color.White && matrix[i - 1 + 500, j].color != Color.Black
                                            && !savedColors.Contains(matrix[i - 1 + 500, j].color)
                                            )
                                        {
                                            if (matrix[i - 1 + 500, j].color == colors[0]) counter2[0]++;
                                            else
                                            {
                                                colors2[1] = matrix[i - 1 + 500, j].color;
                                                counter2[1]++;
                                            }
                                            full = false;
                                        }
                                    }


                                    if (j + 1 < 500 && matrix[i, j + 1].color != Color.White && matrix[i, j + 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j + 1].color)
                                        )
                                    {
                                        if (matrix[i, j + 1].color == colors[0]) counter2[0]++;
                                        else if (matrix[i, j + 1].color == colors[1]) counter2[1]++;
                                        else
                                        {
                                            colors2[2] = matrix[i, j + 1].color;
                                            counter2[2]++;
                                        }
                                        full = false;
                                    }
                                    if (j + 1 > 499)
                                    {
                                        if (matrix[i, j + 1 - 500].color != Color.White && matrix[i, j + 1 - 500].color != Color.Black
                                            && !savedColors.Contains(matrix[i, j + 1 - 500].color)
                                            )
                                        {
                                            if (matrix[i, j + 1 - 500].color == colors[0]) counter2[0]++;
                                            else if (matrix[i, j + 1 - 500].color == colors[1]) counter2[1]++;
                                            else
                                            {
                                                colors2[2] = matrix[i, j + 1 - 500].color;
                                                counter2[2]++;
                                            }
                                            full = false;
                                        }
                                    }


                                    if (j - 1 >= 0 && matrix[i, j - 1].color != Color.White && matrix[i, j - 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i, j - 1].color)
                                        )
                                    {
                                        if (matrix[i, j - 1].color == colors[0]) counter2[0]++;
                                        else if (matrix[i, j - 1].color == colors[1]) counter2[1]++;
                                        else if (matrix[i, j - 1].color == colors[2]) counter2[2]++;
                                        else
                                        {
                                            colors2[3] = matrix[i, j - 1].color;
                                            counter2[3]++;
                                        }
                                        full = false;
                                    }
                                    if (j - 1 < 0)
                                    {
                                        if (matrix[i, j - 1 + 500].color != Color.White && matrix[i, j - 1 + 500].color != Color.Black
                                            && !savedColors.Contains(matrix[i, j - 1 + 500].color)
                                            )
                                        {
                                            if (matrix[i, j - 1 + 500].color == colors[0]) counter2[0]++;
                                            else if (matrix[i, j - 1 + 500].color == colors[1]) counter2[1]++;
                                            else if (matrix[i, j - 1 + 500].color == colors[2]) counter2[2]++;
                                            else
                                            {
                                                colors2[3] = matrix[i, j - 1 + 500].color;
                                                counter2[3]++;
                                            }
                                            full = false;
                                        }
                                    }


                                    maxValue = counter2.Max();
                                    if (maxValue >= 3)
                                    {
                                        int maxIndex = counter2.ToList().IndexOf(maxValue);

                                        if (maxIndex == 0) matrixI[i, j].color = colors2[0];
                                        if (maxIndex == 1) matrixI[i, j].color = colors2[1];
                                        if (maxIndex == 2) matrixI[i, j].color = colors2[2];
                                        if (maxIndex == 3) matrixI[i, j].color = colors2[3];

                                        done = true;
                                    }
                                }

                                if (!done)
                                {
                                    if (i + 1 < 500 && j + 1 < 500 && matrix[i + 1, j + 1].color != Color.White && matrix[i + 1, j + 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1, j + 1].color)
                                        )
                                    {
                                        colors3[0] = matrix[i + 1, j + 1].color;
                                        counter3[0]++; 
                                        full = false;
                                    }
                                    if (i + 1 > 499 && j + 1 > 499)
                                    {
                                        if (matrix[i + 1 - 500, j + 1 - 500].color != Color.White && matrix[i + 1 - 500, j + 1 - 500].color != Color.Black
                                            && !savedColors.Contains(matrix[i + 1 - 500, j + 1 - 500].color)
                                            )
                                        {
                                            colors3[0] = matrix[i + 1 - 500, j + 1 - 500].color;
                                            counter3[0]++;
                                            full = false;
                                        }
                                    }
                                    else if (i + 1 > 499)
                                    {
                                        if (matrix[i + 1 - 500, j].color != Color.White && matrix[i + 1 - 500, j].color != Color.Black
                                            && !savedColors.Contains(matrix[i + 1 - 500, j].color)
                                            )
                                        {
                                            colors3[0] = matrix[i + 1 - 500, j].color;
                                            counter3[0]++;
                                            full = false;
                                        }
                                    }
                                    else if (j + 1 > 499)
                                    {
                                        if (matrix[i, j + 1 - 500].color != Color.White && matrix[i, j + 1 - 500].color != Color.Black
                                            && !savedColors.Contains(matrix[i, j + 1 - 500].color)
                                            )
                                        {
                                            colors3[0] = matrix[i, j + 1 - 500].color;
                                            counter3[0]++;
                                            full = false;
                                        }
                                    }


                                    if (i + 1 < 500 && j - 1 >= 0 && matrix[i + 1, j - 1].color != Color.White && matrix[i + 1, j - 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i + 1, j - 1].color)
                                        )
                                    {
                                        if (matrix[i + 1, j - 1].color == colors3[0]) counter3[0]++;
                                        else
                                        {
                                            colors3[1] = matrix[i + 1, j - 1].color;
                                            counter3[1]++;
                                        }
                                        full = false;
                                    }
                                    if (i + 1 > 499 && j - 1 < 0)
                                    {
                                        if (matrix[i + 1 - 500, j - 1 + 500].color != Color.White && matrix[i + 1 - 500, j - 1 + 500].color != Color.Black
                                            && !savedColors.Contains(matrix[i + 1 - 500, j - 1 + 500].color)
                                            )
                                        {
                                            if (matrix[i + 1 - 500, j - 1 + 500].color == colors3[0]) counter3[0]++;
                                            else
                                            {
                                                colors3[1] = matrix[i + 1 - 500, j - 1 + 500].color;
                                                counter3[1]++;
                                            }
                                            full = false;
                                        }
                                    }
                                    else if (i + 1 > 499)
                                    {
                                        if (matrix[i + 1 - 500, j].color != Color.White && matrix[i + 1 - 500, j].color != Color.Black
                                            && !savedColors.Contains(matrix[i + 1 - 500, j].color)
                                            )
                                        {
                                            if (matrix[i + 1 - 500, j].color == colors3[0]) counter3[0]++;
                                            else
                                            {
                                                colors3[1] = matrix[i + 1 - 500, j].color;
                                                counter3[1]++;
                                            }
                                            full = false;
                                        }
                                    }
                                    else if (j - 1 < 0)
                                    {
                                        if (matrix[i, j - 1 + 500].color != Color.White && matrix[i, j - 1 + 500].color != Color.Black
                                            && !savedColors.Contains(matrix[i, j - 1 + 500].color)
                                            )
                                        {
                                            if (matrix[i, j - 1 + 500].color == colors3[0]) counter3[0]++;
                                            else
                                            {
                                                colors3[1] = matrix[i, j - 1 + 500].color;
                                                counter3[1]++;
                                            }
                                            full = false;
                                        }
                                    }


                                    if (i - 1 >= 0 && j - 1 >= 0 && matrix[i - 1, j - 1].color != Color.White && matrix[i - 1, j - 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1, j - 1].color)
                                        )
                                    {
                                        if (matrix[i - 1, j - 1].color == colors3[0]) counter3[0]++;
                                        else if (matrix[i - 1, j - 1].color == colors3[1]) counter3[1]++;
                                        else
                                        {
                                            colors3[2] = matrix[i - 1, j - 1].color;
                                            counter3[2]++;
                                        }
                                        full = false;
                                    }
                                    if (i - 1 < 0 && j - 1 < 0)
                                    {
                                        if (matrix[i - 1 + 500, j - 1 + 500].color != Color.White && matrix[i - 1 + 500, j - 1 + 500].color != Color.Black
                                            && !savedColors.Contains(matrix[i - 1 + 500, j - 1 + 500].color)
                                            )
                                        {
                                            if (matrix[i - 1 + 500, j - 1 + 500].color == colors3[0]) counter3[0]++;
                                            else if (matrix[i - 1 + 500, j - 1 + 500].color == colors3[1]) counter3[1]++;
                                            else
                                            {
                                                colors3[2] = matrix[i - 1 + 500, j - 1 + 500].color;
                                                counter3[2]++;
                                            }
                                            full = false;
                                        }
                                    }
                                    else if (i - 1 < 0)
                                    {
                                        if (matrix[i - 1 + 500, j].color != Color.White && matrix[i - 1 + 500, j].color != Color.Black
                                            && !savedColors.Contains(matrix[i - 1 + 500, j].color)
                                            )
                                        {
                                            if (matrix[i - 1 + 500, j].color == colors3[0]) counter3[0]++;
                                            else if (matrix[i - 1 + 500, j].color == colors3[1]) counter3[1]++;
                                            else
                                            {
                                                colors3[2] = matrix[i - 1 + 500, j].color;
                                                counter3[2]++;
                                            }
                                            full = false;
                                        }
                                    }
                                    else if (j - 1 < 0)
                                    {
                                        if (matrix[i, j - 1 + 500].color != Color.White && matrix[i, j - 1 + 500].color != Color.Black
                                            && !savedColors.Contains(matrix[i, j - 1 + 500].color)
                                            )
                                        {
                                            if (matrix[i, j - 1 + 500].color == colors3[0]) counter3[0]++;
                                            else if (matrix[i, j - 1 + 500].color == colors3[1]) counter3[1]++;
                                            else
                                            {
                                                colors3[2] = matrix[i, j - 1 + 500].color;
                                                counter3[2]++;
                                            }
                                            full = false;
                                        }
                                    }


                                    if (i - 1 >= 0 && j + 1 < 500 && matrix[i - 1, j + 1].color != Color.White && matrix[i - 1, j + 1].color != Color.Black
                                        && !savedColors.Contains(matrix[i - 1, j + 1].color)
                                        )
                                    {
                                        if (matrix[i - 1, j + 1].color == colors3[0]) counter3[0]++;
                                        else if (matrix[i - 1, j + 1].color == colors3[1]) counter3[1]++;
                                        else if (matrix[i - 1, j + 1].color == colors3[2]) counter3[2]++;
                                        else
                                        {
                                            colors3[3] = matrix[i - 1, j + 1].color;
                                            counter3[3]++;
                                        }
                                        full = false;
                                    }
                                    if (i - 1 < 0 && j + 1 > 499)
                                    {
                                        if (matrix[i - 1 + 500, j + 1 - 500].color != Color.White && matrix[i - 1 + 500, j + 1 - 500].color != Color.Black
                                            && !savedColors.Contains(matrix[i - 1 + 500, j + 1 - 500].color)
                                            )
                                        {
                                            if (matrix[i - 1 + 500, j + 1 - 500].color == colors3[0]) counter3[0]++;
                                            else if (matrix[i - 1 + 500, j + 1 - 500].color == colors3[1]) counter3[1]++;
                                            else if (matrix[i - 1 + 500, j + 1 - 500].color == colors3[2]) counter3[2]++;
                                            else
                                            {
                                                colors3[3] = matrix[i - 1 + 500, j + 1 - 500].color;
                                                counter3[3]++;
                                            }
                                            full = false;
                                        }
                                    }
                                    else if (i - 1 < 0)
                                    {
                                        if (matrix[i - 1 + 500, j].color != Color.White && matrix[i - 1 + 500, j].color != Color.Black
                                            && !savedColors.Contains(matrix[i - 1 + 500, j].color)
                                            )
                                        {
                                            if (matrix[i - 1 + 500, j].color == colors3[0]) counter3[0]++;
                                            else if (matrix[i - 1 + 500, j].color == colors3[1]) counter3[1]++;
                                            else if (matrix[i - 1 + 500, j].color == colors3[2]) counter3[2]++;
                                            else
                                            {
                                                colors3[3] = matrix[i - 1 + 500, j].color;
                                                counter3[3]++;
                                            }
                                            full = false;
                                        }
                                    }
                                    else if (j + 1 > 499)
                                    {
                                        if (matrix[i, j + 1 - 500].color != Color.White && matrix[i, j + 1 - 500].color != Color.Black
                                            && !savedColors.Contains(matrix[i, j + 1 - 500].color)
                                            )
                                        {
                                            if (matrix[i, j + 1 - 500].color == colors3[0]) counter3[0]++;
                                            else if (matrix[i, j + 1 - 500].color == colors3[1]) counter3[1]++;
                                            else if (matrix[i, j + 1 - 500].color == colors3[2]) counter3[2]++;
                                            else
                                            {
                                                colors3[3] = matrix[i, j + 1 - 500].color;
                                                counter3[3]++;
                                            }
                                            full = false;
                                        }
                                    }

                                    maxValue = counter3.Max();
                                    if (maxValue >= 3)
                                    {
                                        int maxIndex = counter3.ToList().IndexOf(maxValue);

                                        if (maxIndex == 0) matrixI[i, j].color = colors3[0];
                                        if (maxIndex == 1) matrixI[i, j].color = colors3[1];
                                        if (maxIndex == 2) matrixI[i, j].color = colors3[2];
                                        if (maxIndex == 3) matrixI[i, j].color = colors3[3];

                                        done = true;
                                    }

                                }

                                if (!done)
                                {

                                    //checking if percentage is ok
                                    if (!Int32.TryParse(percentageTextBox.Text, out int percentage))
                                    {
                                        MessageBox.Show("Bad value. Please fix it.", "Percentage", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return;
                                    }


                                   
                                    int chance = rnd.Next(0, 100);

                                    if (chance <= percentage)
                                    {
                                        maxValue = counter.Max();
                                        int maxIndex = counter.ToList().IndexOf(maxValue);

                                        if (maxIndex == 0) matrixI[i, j].color = colors[0];
                                        if (maxIndex == 1) matrixI[i, j].color = colors[1];
                                        if (maxIndex == 2) matrixI[i, j].color = colors[2];
                                        if (maxIndex == 3) matrixI[i, j].color = colors[3];
                                        if (maxIndex == 4) matrixI[i, j].color = colors[4];
                                        if (maxIndex == 5) matrixI[i, j].color = colors[5];
                                        if (maxIndex == 6) matrixI[i, j].color = colors[6];
                                        if (maxIndex == 7) matrixI[i, j].color = colors[7];

                                        done = true;
                                    }

                                }

                                for (int k = 0; k < 8; k++)
                                {
                                    colors[k] = Color.White;
                                    counter[k] = 0;
                                }
                                for (int k = 0; k < 4; k++)
                                {
                                    counter2[k] = 0;
                                    counter3[k] = 0;
                                }

                            }
                        }
                    }

                    // replace matrixes and set bitmap
                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            matrix[i, j].color = matrixI[i, j].color;
                        }
                    }

                    for (int i = 0; i < 500; i++)
                    {
                        for (int j = 0; j < 500; j++)
                        {
                            bitmap.SetPixel(i, j, matrixI[i, j].color);
                        }
                    }

                    pictureBox1.Image = bitmap;
                    pictureBox1.Refresh();
                }
            }


            //inclusions after
            if (AfterCheckBox.Checked) inclusions();

            first = true;
            full = false;
            ready = true;
        }

        private void textExportMenu_Click(object sender, EventArgs e)
        {
            //checking if structure is OK to export
            if (!ready)
            {
                MessageBox.Show("Properties structure isn't ready. Please fix it.", "Structure export", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "|*.txt";
            dialog.ShowDialog(); 

            System.IO.FileStream fs = System.IO.File.Create(dialog.FileName);

            //write txt file
            using (System.IO.StreamWriter w = new System.IO.StreamWriter(fs))
            {
                for (int i = 0; i < 500; i++)
                {
                    for (int j = 0; j < 500; j++)
                    {
                        w.Write(i + " ");
                        w.Write(j + " ");
                        w.Write(matrix[i, j].color.Name + "");
                        w.WriteLine();
                    }

                }
            }

            if (System.IO.File.Exists(dialog.FileName))
            {
                MessageBox.Show("Export txt - ok.", "Structure export", MessageBoxButtons.OK, MessageBoxIcon.None);
                return;
            }
        }

        private void bitmapExportMenu_Click(object sender, EventArgs e)
        {
            //checking if structure is OK to export
            if (!ready)
            {
                MessageBox.Show("Properties structure isn't ready. Please fix it.", "Structure export", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            SaveFileDialog dialog = new SaveFileDialog();
            dialog.Filter = "|*.bmp";
            dialog.ShowDialog();

            bitmap.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Bmp);

            if (System.IO.File.Exists(dialog.FileName))
            {
                MessageBox.Show("Export bmp - ok.", "Structure export", MessageBoxButtons.OK, MessageBoxIcon.None);
                return;
            }
        }

        private void bitmapImportMenu_Click(object sender, EventArgs e)
        {
            //initialization of tables
           for (int i = 0; i < 500; i++)
            {
                for (int j = 0; j < 500; j++)
                {
                    matrix[i, j] = new Grain();
                    matrix[i, j].color = Color.White;
                }
            }

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.ShowDialog();

            Bitmap bmp = new Bitmap(dialog.FileName);

            for(int i = 0; i < 500; i++)
            {
                for(int j = 0; j < 500; j++)
                {
                    matrix[i, j].color = bmp.GetPixel(i,j);
                }
            }

            pictureBox1.Image = bmp;
            pictureBox1.Refresh();
        }

        private void textImportMenu_Click(object sender, EventArgs e)
        {
            //initialization of tables
            for (int i = 0; i < 500; i++)
            {
                for (int j = 0; j < 500; j++)
                {
                    matrix[i, j] = new Grain();
                    matrix[i, j].color = Color.White;
                }
            }

            OpenFileDialog dialog = new OpenFileDialog();
            dialog.ShowDialog();

            string[] lines = System.IO.File.ReadAllLines(dialog.FileName);

            int a=0;
            int r=0;
            int g=0;
            int b=0;
            int counter = 0;

            for (int i = 0; i < 500; i++)
            {
                for (int j = 0; j < 500; j++)
                {
                    string[] data = lines[counter].Split(' ');
                   a= Int32.Parse(data[2].Substring(0, 2), NumberStyles.HexNumber);
                    r=Int32.Parse(data[2].Substring(2, 2), NumberStyles.HexNumber);
                    g=Int32.Parse(data[2].Substring(4, 2), NumberStyles.HexNumber);
                    b=Int32.Parse(data[2].Substring(6, 2), NumberStyles.HexNumber);
                    matrix[i, j].color = Color.FromArgb(a,r,g,b);
                    counter++;
                }
            }


            // setting bitmap
            for (int i = 0; i < 500; i++)
            {
                for (int j = 0; j < 500; j++)
                {
                    bitmap.SetPixel(i, j, matrix[i, j].color);
                }
            }

            pictureBox1.Image = bitmap;
            pictureBox1.Refresh();

        }

        private void inclusions()
        {
            //checking if amount of inclusions is ok
            if (!Int32.TryParse(amountOfInclusionsTextBox.Text, out int amountOfInclusions))
            {
                MessageBox.Show("Bad value - amount of inclusions. Please fix it.", "Amount of inclusions", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //checking if size of inclusions is ok
            if (!Int32.TryParse(sizeOfInclusionsTextBox.Text, out int sizeOfInclusions))
            {
                MessageBox.Show("Bad value - size of inclusions. Please fix it.", "Size of inclusions", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int[] pointx = new int[amountOfInclusions];
            int[] pointy = new int[amountOfInclusions];

            if (BeforeCheckBox.Checked)
            {
                //randomly choosing inclusions
               
                for (int k = 0; k < amountOfInclusions; k++)
                {
                    int i = rnd.Next(0+(sizeOfInclusions), 500-(sizeOfInclusions));
                    int j = rnd.Next(0 + (sizeOfInclusions), 500 - (sizeOfInclusions));
                    matrix[i, j].color = Color.Black;
                    pointx[k] = i;
                    pointy[k] = j;
                }

                if (SquareCheckBox.Checked)
                {
                    for(int i = 0; i < amountOfInclusions; i++)
                    {
                        for (int j = 0; j < sizeOfInclusions; j++)
                        {
                            for (int k = 0; k < sizeOfInclusions; k++)
                            {
                                matrix[pointx[i]+j, pointy[i]+k].color = Color.Black;
                            }
                        }
                    }
                }

                if (CircularCheckBox.Checked)
                {
                    for(int i = 0; i < amountOfInclusions; i++)
                    {
                        for(int j=0; j<500; j++)
                        {
                            for(int k=0; k<500; k++)
                            {
                                double var = Math.Sqrt(  (Math.Pow((pointx[i] - j), 2.0)) + (Math.Pow((pointy[i] - k), 2.0))   );

                                if ( var < (sizeOfInclusions/2.0)){
                                    matrix[j, k].color = Color.Black;
                                }
                            }
                        }
                    }
                }

            }

            if (AfterCheckBox.Checked)
            {
                if (SquareCheckBox.Checked)
                {
                   
                    int i = 0;
                    while(pointx[amountOfInclusions-1]==0)
                    {
                        int j = rnd.Next(0 + (sizeOfInclusions / 2), 500 - (sizeOfInclusions));
                        int k = rnd.Next(0 + (sizeOfInclusions / 2), 500 - (sizeOfInclusions));

                        if (matrix[j+1, k].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j+1, k+1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j, k+1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j-1, k+1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j-1, k].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j-1, k-1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j, k-1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j+1, k-1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        
                    }

                    for(int l = 0; l < amountOfInclusions; l++)
                    {
                        matrix[pointx[l], pointy[l]].color = Color.Black;
                    }

                    for (int j = 0; j < amountOfInclusions; j++)
                    {
                        for (int k = 0; k < sizeOfInclusions; k++)
                        {
                            for (int l = 0; l < sizeOfInclusions; l++)
                            {
                                matrix[pointx[j] + k, pointy[j] + l].color = Color.Black;
                            }
                        }
                    }



                }

                if (CircularCheckBox.Checked)
                {
                    
                    int i = 0;
                    while (pointx[amountOfInclusions - 1] == 0)
                    {
                        int j = rnd.Next(0 + (sizeOfInclusions), 500 - (sizeOfInclusions));
                        int k = rnd.Next(0 + (sizeOfInclusions), 500 - (sizeOfInclusions));

                        if (matrix[j + 1, k].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j + 1, k + 1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j, k + 1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j - 1, k + 1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j - 1, k].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j - 1, k - 1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j, k - 1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }
                        else if (matrix[j + 1, k - 1].color != matrix[j, k].color)
                        {
                            pointx[i] = j;
                            pointy[i] = k;
                            i++;
                        }

                    }

                    for (int l = 0; l < amountOfInclusions; l++)
                    {
                        matrix[pointx[l], pointy[l]].color = Color.Black;
                    }

                    for (int j = 0; j < amountOfInclusions; j++)
                    {
                        for (int k = 0; k < 500; k++)
                        {
                            for (int l = 0; l < 500; l++)
                            {
                                double var = Math.Sqrt((Math.Pow((pointx[j] - k), 2.0)) + (Math.Pow((pointy[j] - l), 2.0)));

                                if (var < (sizeOfInclusions / 2.0))
                                {
                                    matrix[k, l].color = Color.Black;
                                }
                            }
                        }
                    }


                }


                for (int i = 0; i < 500; i++)
                {
                    for (int j = 0; j < 500; j++)
                    {
                        bitmap.SetPixel(i, j, matrix[i, j].color);
                    }
                }

                pictureBox1.Image = bitmap;
                pictureBox1.Refresh();

            }

        }


        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            Bitmap bmp = new Bitmap(pictureBox1.Image);
            savedGrains[e.X,e.Y].color = bmp.GetPixel(e.X, e.Y);
            savedColors[numberSaved] = savedGrains[e.X, e.Y].color;
            numberSaved++;
        }

        private void ClearAllButton_Click(object sender, EventArgs e)
        {
            saved = false;
            ready = false;
            first = true;
            full = false;                       
            firstSaved = true;
            numberSaved = 0;
            boundariesSize = 0;
            boundariesPercentage = 0;

            GBLabel.Text = "" + boundariesPercentage;

            for (int i=0; i<500; i++)
            {
                for(int j=0; j<500; j++)
                {
                    matrix[i, j].color = Color.White;
                    matrixI[i, j].color = Color.White;
                    bitmap.SetPixel(i, j, Color.White);
                    savedColors[i] = Color.White;
                    savedGrains[i, j] = new Grain();
                }
            }

            pictureBox1.Image = bitmap;
            pictureBox1.Refresh();

        }
    }


    //Grain class
    public class Grain
    {
        public Color color;
        public int phase;
    }

}
